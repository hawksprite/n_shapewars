using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class CFX_AutoDestructShuriken : MonoBehaviour
{
	public bool OnlyDeactivate;

	public bool isNetworked = true;
	
	void OnEnable()
	{
		StartCoroutine("CheckIfAlive");
	}
	
	IEnumerator CheckIfAlive ()
	{
		while(true)
		{
			yield return new WaitForSeconds(0.5f);

            // This is in every particle, so use this to check if we're to far from the camera
            if (Vector3.Distance(transform.position, PlayerStats.Player.CameraLocation) > GlobalConfiguration.ObjectViewDistance)
            {
                GameObject.Destroy(this.gameObject);
            }

			if(!particleSystem.IsAlive(true))
			{
				if(OnlyDeactivate)
				{
					#if UNITY_3_5
						this.gameObject.SetActiveRecursively(false);
					#else
						this.gameObject.SetActive(false);
					#endif
				}
				else
                    if (isNetworked)
                    {
                        PhotonNetwork.Destroy(this.gameObject);
                    }
                    else
                    {
                        GameObject.Destroy(this.gameObject);
                    }
				break;
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Worker : MonoBehaviour {

    private Entity entity;

    private List<Vectorite> resourceTargets = new List<Vectorite>();

    private Vectorite resource;
    private CC home;

    [HideInInspector]
    public float Haul = 0.0f;
    public float MaxHaul = 10.0f;
    [HideInInspector]
    public float WorkDistance = 1.5f;
    [HideInInspector]
    public float DumpDistance = 3.0f;

    public float nodeDistance = 15.0f;

    public bool isGathering = true;

    public Vector3 localSpawnLocation
    {
        get
        {
            return new Vector3(transform.position.x, 0, transform.position.z);
        }
    }

	// Use this for initialization
	void Start () {

        entity = GetComponent<Entity>();

        FindResource();
        FindHome();

        // Raise up a unit
        transform.position = transform.position + new Vector3(0, 0, 0);

        lastHaulTime = DateTime.Now;
	}

    public void OnUserAction(string action)
    {
        // Recieve actions from the parent entity, useful for breaking from harvest on a move request.
        if (action == "move")
        {
            // If we're in gather right now pull them off it
            isGathering = false;
        }
    }

    void OnAction(string action)
    {
        // Check for a return to gather action
        if (action == "gather")
        {
            // Re find resources
            FindResource();
            FindHome();

            // However if it's an action to gather begin gathering
            isGathering = true;
        }

        // Certain actions can't be used while in gather mode
        if (isGathering == false)
        {
            if (action == "build-factory")
            {
                
				if (PlayerStats.instance._player.Faction == 0){
					GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CUBE_Factory, localSpawnLocation, Quaternion.identity, null);
				}
				if (PlayerStats.instance._player.Faction == 1){
					GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Barracks, localSpawnLocation, Quaternion.identity, null);
				}
				if (PlayerStats.instance._player.Faction == 2){
					GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Barracks, localSpawnLocation, Quaternion.identity, null);
				}
            }
            if (action == "build-cc")
            {
                
				if (PlayerStats.instance._player.Faction == 0){
					GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CUBE_Command, localSpawnLocation, Quaternion.identity, null);
				}
				if (PlayerStats.instance._player.Faction == 1){
					GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Command, localSpawnLocation, Quaternion.identity, null);
				}
				if (PlayerStats.instance._player.Faction == 2){
					GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Command, localSpawnLocation, Quaternion.identity, null);
				}
            }

			if (action == "build-turret")
			{
				if (PlayerStats.instance._player.Faction == 0){
					GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CUBE_Turret, localSpawnLocation, Quaternion.identity, null);
				}
				if (PlayerStats.instance._player.Faction == 1){
					GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Turret, localSpawnLocation, Quaternion.identity, null);
				}
				if (PlayerStats.instance._player.Faction == 2){
					GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Turret, localSpawnLocation, Quaternion.identity, null);
				}
			}
            /*
            if (action == "build-factory")
            {
                // If the player can afford it destroy your self and spawn a factory
                if (Stats.Purchase(100))
                {
                    GeneralControll.instance.SpawnUnit(UnitCatalog.Units.Cuboid_Barracks, localSpawnLocation, Quaternion.identity, null);
                }
            }

            if (action == "build-defense")
            {
                // If the player can afford it destroy your self and spawn a factory
                if (Stats.Purchase(125))
                {
                    GeneralControll.instance.SpawnUnit(UnitCatalog.Units.Cuboid_Turret, localSpawnLocation, Quaternion.identity, null);
                }
            }

            if (action == "build-cc")
            {
                if (Stats.Purchase(400))
                {
                    GeneralControll.instance.SpawnUnit(UnitCatalog.Units.Cuboid_CommandCenter, localSpawnLocation, Quaternion.identity, null);
                }
            }*/
        }
    }

    void FindResource()
    {
        resourceTargets.Clear();

        GameObject[] vectors = GameObject.FindGameObjectsWithTag("Vector");

        GameObject closest = vectors[0];

        for (int i = 0; i < vectors.Length; i++)
        {
            float Length = Vector3.Distance(transform.position, vectors[i].transform.position);
            float Close = nodeDistance;//Vector3.Distance(transform.position, closest.transform.position);
            Vectorite v = vectors[i].GetComponent<Vectorite>();

            if (v.Empty == false)
            {
                if (Length <= Close)
                {
                    resourceTargets.Add(v);
                }
            }
        }

        int rnd = UnityEngine.Random.Range(0, resourceTargets.Count);

        resource = resourceTargets[rnd];
    }

    void FindHome()
    {
        GameObject[] vectors = GameObject.FindGameObjectsWithTag("CC");

        GameObject closest = vectors[0];

        for (int i = 0; i < vectors.Length; i++)
        {
            CC cc = vectors[i].GetComponent<CC>();

            if (cc)
            {
                if (cc.entity.currentSide == Entity.Side.Friendly)
                {
                    float Length = Vector3.Distance(transform.position, vectors[i].transform.position);
                    float Close = Vector3.Distance(transform.position, closest.transform.position);

                    if (Length < Close)
                    {
                        closest = vectors[i];
                    }
                }
            }
        }

        home = closest.GetComponent<CC>();
    }
	
	// Update is called once per frame
    int haulTimer = 0;
    DateTime lastHaulTime;
	void Update () {


	    if (entity.locallyOwned)
        {
            if (isGathering)
            {

                // Are we full?
                if (Haul >= MaxHaul)
                {
                    // Go Home!
                    if (Vector3.Distance(transform.position, home.transform.position + new Vector3(0, 0, 0)) > DumpDistance)
                    {
                        entity.MoveTo(home.transform.position + new Vector3(0,0, 0));
                    }
                    else
                    {
                        // Freeze
                        entity.MoveTo(transform.position);

                        // Dump it
                        home.AcceptResources((int)Haul);
                        Haul = 0.0f;
                    }
                }
                else
                {
                    if (Vector3.Distance(transform.position, resource.transform.position + new Vector3(0, 0, 0)) > WorkDistance)
                    {
                        entity.MoveTo(resource.transform.position);
                    }
                    else
                    {
                        // Freeze
                        entity.MoveTo(transform.position);

                        // Based on a haul timer recieve resources
                        if ((DateTime.Now - lastHaulTime).Seconds >= 2)
                        {
                            lastHaulTime = DateTime.Now;

                            // Pull the yield off our resource
                            Haul += resource.GetYield(MaxHaul);
                        }


                        // DOn't let them get more then the max haul
                        if (Haul >= MaxHaul)
                        {
                            Haul = MaxHaul;
                        }
                    }
                }
            }
        }
	}
}

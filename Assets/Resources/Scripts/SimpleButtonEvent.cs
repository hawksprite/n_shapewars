﻿using UnityEngine;
using System.Collections;

public class SimpleButtonEvent : MonoBehaviour {

    public string ButtonAction = "default";

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnClick()
    {
        // Alert the MainCamera (Whatever controlling scripts for the scenes should be attached ot it)
        GameObject.FindGameObjectWithTag("MainCamera").SendMessage("HadAction", ButtonAction);
    }

    void OnSubmit()
    {
        GameObject.FindGameObjectWithTag("MainCamera").SendMessage("HadAction", ButtonAction);
        Debug.Log("Submitted");
    }
}

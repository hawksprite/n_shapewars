﻿using UnityEngine;
using System.Collections;

public static class Stats {

    public static PlayerStats playerStats; // Our local player stats object

    public static void Prepare()
    {
        // This is called from the PlayerStats object when it is spawned and attached to this interface
    }

    public static int VectoriteBalance // This is really just pointing back to the specific balance of our local PSS
    {
        get {
            if (playerStats)
            {
                return playerStats._player.Vectorites;
            }
            else
            {
                return 0; // Whatev's dog
            }
        }
    }

    public static void RecieveVectorites(int ammount)
    {
        // Add them to our actual balance
        playerStats._player.Vectorites += ammount;

        // Also store them in our global total
        playerStats._player.TotalIncome += ammount;
    }

    public static void SetVectorites(int ammount)
    {
        playerStats._player.Vectorites = ammount;
    }

    public static bool Purchase(int cost)
    {
        if (ownedUnits >= GlobalConfiguration.UnitCap)
        {
            return false; // Ban purchasing of units here no matter what.
        }

        if (VectoriteBalance >= cost)
        {
            playerStats._player.Vectorites -= cost;
            return true;
        }
        else
            return false;
    }

    public static int ownedUnits
    {
        get
        {
            return EntityManager.getAllyList().Count;
        }
    }
}

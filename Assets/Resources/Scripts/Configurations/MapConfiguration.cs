﻿using UnityEngine;
using System.Collections;

public class MapConfiguration : MonoBehaviour {
    // This should be setup in the editor so it lives in the instance of each scene.

    // It's just a clean way to hold any You guessed it! Map configuration related variables

    public float MaxPlayers = 4;

    public Vector3[] SpawnLocations;

    // Some utility functions dealing with the vars
    public Vector3 GetRandomSpawnLocation()
    {
        if (SpawnLocations.Length == 0)
        {
            // If for some reason this doesn't have any spawn locations just shoot back a Empty vector
            return Vector3.zero;
        }
        else
        {
            // Our index should just be your position in the lobby list to avoid spawning in the same spot.

            // Use our player stats to find an empty slots, hopefully preventing from spawning on another player
            int index = PlayerStats.Player.SpawnSlot;

            return SpawnLocations[index];
        }
    }
}

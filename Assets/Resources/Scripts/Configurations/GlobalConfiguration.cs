﻿using UnityEngine;
using System.Collections;

public static class GlobalConfiguration {

	// Full scope static singleton to house the actual game configuration
    public static bool EnabledPostProcesses = true;
    public static float CameraMoveSpeed = 55.0f;
    public static float MaxCameraMoveSpeed = 200.0f;
    public static float EntityRangeOperator = 2.05f;
    public static bool ShowEnemyOnMinimap = false;
    public static string ClientVersionIdentifier = "Release 4.1";
    public static int NetworkFPSCheckTime = 4;
    public static int UnitCap = 200;
    public static bool DestroyedByPhotonNetworkOrQuit = true;
    public static float RallyPointOffsetRadius = 5.0f; // How many units should the random offset radius for Rally points be on spawn.
    public static bool AllowCheats = false;
	public static int WorkerSyncDelayMilliseconds = 300;
    public static int ObjectViewDistance = 80;
    public static float Volume_BackgroundMusic = 0.1f;

    // Network/Package manager settings.  Useful to tweak for polishing
    public static int NetworkMaxPackageQue = 100; // Max size for our packet que
    public static int NetworkPackageMillisecondDelay = 1;
    public static int NetworkMaxPacketsPerPackage = 40;

    // Takes a value, it's min then convert's it to a number between 0.0 and 1.0
    public static float ConvertToFloatRange(float max, float value)
    {
        return value / max;
    }

    // Takes a number from 0.0 to 1.0 and using a given max scales it back up
    public static float ConvertFromFloatRange(float max, float value)
    {
        // Value check
        if (value > 1.0f || value < 0.0f) { 
            return 0.0f; // Not in the proper range 
        }

        return value * max;
    }
}

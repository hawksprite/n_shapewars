﻿using UnityEngine;
using System.Collections;

public class RotateOnY : MonoBehaviour {

    public float Speed = 10.0f;

	void Update () {
        this.transform.Rotate(new Vector3(0, Speed * Time.smoothDeltaTime, 0));
	}
}

﻿using UnityEngine;
using System.Collections;

public class GeneralSync : Photon.MonoBehaviour {

	// Very thin and simple GeneralSync script for attaching to photon view, good for particles and a starting point for networked objects.

	public bool lerpValuesIn = false; // Setting this to true will lerp the values in instead of just steting them

    public bool forceRotationFromNet = false;
    public Vector3 netRotationSet;

	void Awake()
	{
		gameObject.name = gameObject.name + photonView.viewID;
	}
	
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		// Get and set locations and rotations
		if (stream.isWriting)
		{
			stream.SendNext(0);
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation); 
		}
		else
		{
			//Network player, receive data
			int t = (int)stream.ReceiveNext();
			correctEPos = (Vector3)stream.ReceiveNext();
			correctERot = (Quaternion)stream.ReceiveNext();
		}
	}
	
	private Vector3 correctEPos = Vector3.zero; //We lerp towards this
	private Quaternion correctERot = Quaternion.identity; //We lerp towards this
	
	void Update()
	{
		if (!photonView.isMine)
		{
			if (lerpValuesIn){
				transform.position = Vector3.Lerp(transform.position, correctEPos, Time.deltaTime * 5);

                if (forceRotationFromNet) { transform.rotation = Quaternion.Euler(netRotationSet); }
                else
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, correctERot, Time.deltaTime * 5);
                }

			}
			else{
				transform.position = correctEPos;

                if (forceRotationFromNet) { transform.rotation = Quaternion.Euler(netRotationSet); }
                else
                {
                    transform.rotation = correctERot;
                }
			}
		}
	}
}

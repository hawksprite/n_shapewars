﻿using UnityEngine;
using System.Collections;
using System;

public class EntitySync : Photon.MonoBehaviour {

	private Entity entity;
    private bool InitialSerilization = false;
    public float SnapThreshold = 10.0f;
    private bool enemyColorSet = false;
    private Vector3 correctEPos = Vector3.zero; //We lerp towards this
    private Vector3 entityTargetLatest = Vector3.zero;
    private Quaternion correctERot = Quaternion.identity; //We lerp towards this
	private float correctYRot = 0;

	[HideInInspector]
	public bool isWorker = false;

	void Awake()
	{
		if (!PhotonNetwork.connected) {
			// If we're not connected kick back to menu
			Application.LoadLevel (0);
			return;
		}

        gameObject.name += "-VIEW_ID-" + photonView.viewID.ToString();

        photonView.destroyedByPhotonNetworkOrQuit = GlobalConfiguration.DestroyedByPhotonNetworkOrQuit;

		entity = GetComponent<Entity> ();

        // Initalize the main entity
        entity.Initialize(photonView.isMine, photonView.isMine);

		if (!photonView.isMine) {
			entity.settings.mainRenderer.enabled = false;

            // Hide all the child render'ers as well
            MeshRenderer[] r = GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer m in r)
            {
                RangeInterface rInterface = m.gameObject.GetComponent<RangeInterface>();

                if (!rInterface)
                {
                    m.enabled = false;
                }
            }
		}

		lastLocation = transform.position;
		lastRotation = transform.rotation.y;

		Worker w = GetComponent<Worker> ();
		isWorker = false;

		//if (w) { isWorker = true; }
	}

	DateTime lastUpdate = DateTime.Now;
	bool updatedThisFrame = false;
	bool UpdateInfo()
	{
		if (isWorker) {
			if ((DateTime.Now - lastUpdate).TotalMilliseconds < GlobalConfiguration.WorkerSyncDelayMilliseconds)
			{
				return false;
			}
			else{
				// There's been long enough
				lastUpdate = DateTime.Now;
			}
		}

		if (!updatedThisFrame) {
			updatedThisFrame = true;
			string buffer = gameObject.name + "|" + NetworkTools.Vector3ToString (transform.position) + "|" + transform.rotation.eulerAngles.y.ToString () + "|" + NetworkTools.Vector3ToString (entity.targetLocation);

            StartCoroutine("_crSubmitBuffer", buffer);

            // Once a packet is sent, record the value's so we can compare since last send
            lastLocation = transform.position;
            lastRotation = transform.rotation.eulerAngles.y ;
		}

		return true;
	}

    void _crSubmitBuffer(string buffer)
    {
        while (_crIsReady() == false) { } // Wait till the EntityPacketManager is ready
        EntityPacketManager.instance.SendBuffer(buffer);
    }

    bool _crIsReady() { if (EntityPacketManager.instance) { return true; } else { return false; } }

    bool remoteFirstRecieve = false;

    void RecieveInfo(string data)
    {
        string[] bits = data.Split('|');

        string from = "";
        string unitName = "";

        try
        {
            from = bits[0];
            unitName = bits[1];
            correctEPos = NetworkTools.Vector3FromString(bits[2]);
            correctYRot = float.Parse(bits[3]);
            entityTargetLatest = NetworkTools.Vector3FromString(bits[4]);

            correctEPos.y        = 0;
            entityTargetLatest.y = 0;
        }
        catch (System.Exception z)
        { }

        if (remoteFirstRecieve == false)
        {
            remoteFirstRecieve = true;

            transform.position = correctEPos;
            transform.rotation = Quaternion.Euler(new Vector3(0, correctYRot, 0));

            entity.setOverlayColor(PlayerStats.instance.GetPSS(from).Color);

            // Re enable the main render
            entity.settings.mainRenderer.enabled = true;
            entity.OPT_Show();
        }
    }
    
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		/*
		if (stream.isWriting)
		{
			//We own this player: send the others our data
			stream.SendNext(0);
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation); 
		}
		else
		{
			//Network player, receive data
			int t = (int)stream.ReceiveNext();
			correctEPos = (Vector3)stream.ReceiveNext();
			correctERot = (Quaternion)stream.ReceiveNext();

            if (InitialSerilization == false)
            {
                // If this was the first serilization snap the object there
                transform.position = correctEPos;
                transform.rotation = correctERot;

                InitialSerilization = true;
            }
            else
            {
                // Otherwise check for the "Snap" threshold, if the objects new location is to far away snap the object there.
                float changeDistance = Vector3.Distance(correctEPos, transform.position);

                if (changeDistance >= SnapThreshold)
                {
                    transform.position = correctEPos;
                    transform.rotation = correctERot;
                }
            }

            // On serilization also set color if it hasn't been yet
            if (enemyColorSet == false)
            {
                enemyColorSet = true;

                PSS enemyPSS = PlayerStats.instance.GetPSS(info.sender.name);

                entity.InitalizeColor(enemyPSS.Color);
            }
		} */
	}



	Vector3 lastLocation;
	float lastRotation;

	bool local_FirstSender = false;


    DateTime grandSync = DateTime.Now;

	void Update()
	{
		updatedThisFrame = false;

		if (!photonView.isMine) {
			//Update remote player (smooth this, this looks good, at the cost of some accuracy)

			transform.position = Vector3.Lerp (transform.position, correctEPos, Time.deltaTime * 5);


            // If it's not a building apply some extra position prediction
            if (entity.settings.IsBuilding == false)
            {
                // If they're reached that position, and there's still a distance to go for the last recieved end target, continue moving towards the target to simulate less lag
                if (Vector3.Distance(transform.position, correctEPos) < 1.0f)
                {
                    // Move towards it's end target
                    transform.position = Vector3.Lerp(transform.position, entityTargetLatest, Time.deltaTime * 5);
                }
            }

		    float newYRotation = Mathf.LerpAngle(transform.rotation.y, correctYRot, Time.deltaTime * 5);
			transform.rotation = Quaternion.Euler(new Vector3(0, correctYRot, 0));
		} else {
			// Determine when we should send info

			if (!local_FirstSender){
				local_FirstSender = true;

				// Do a manual first send
				string buffer = gameObject.name + "|" + NetworkTools.Vector3ToString (transform.position) + "|" + transform.rotation.y.ToString ();

                EntityPacketManager.instance.SendBuffer(buffer);
			}
			else{
				if (entity.currentState != Entity.States.Idle){

					if (Vector3.Distance(lastLocation, transform.position) > 0.1f)
					{
						UpdateInfo();
					}
				}

                // Check for the "GrandSync", ever second just send an info update to make sure it's out there.
                if ((DateTime.Now - grandSync).TotalSeconds >= 2)
                {
                    grandSync = DateTime.Now;

                    UpdateInfo();
                }
			}
		}
	}

    public void NetworkBroadcastWeapon(string data)
    {
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            if (!PhotonNetwork.playerList[i].isLocal)
            {
                photonView.RPC("NetworkFireWeapon", PhotonNetwork.playerList[i], data);
            }
        }
    }

    [RPC]
    void NetworkFireWeapon(string data, PhotonMessageInfo info)
    {
        entity.EmulateFireWeapon(data);
    }

}

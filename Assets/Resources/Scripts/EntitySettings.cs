﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EntitySettings : MonoBehaviour {

    // This is a settings holder for the main entity scripts, almost entirely variables with a few "Helper" functions for info.

    // Public settings
    public string DisplayName = "Unit";
    public MeshRenderer mainRenderer;
    public GameObject entityWeapon;
	public Vector3 RotationOffset = new Vector3(0,90,0);
    public float MovementSpeed = 20.0f;
    public int MaxHealth = 300;
    public float AttackRange = 30.0f;
    public float AttackSpeed = 400.0f;
    public int AttackDamage = 20;
	public float ObstacleAvoidanceRadius = 0.5f;
    public int Cost = 200;
    public bool IsBuilding = false;
    public bool RotateOnMovement = true;
    public bool SampleNavMesh = true;
    public bool AutoAgro = true;
    public bool SendNotifications = false;
	public bool AutoWeapon = true;
	public AudioClip spawnSound;

    // Buttons
    public List<EntityButtonItem> entityButtons = new List<EntityButtonItem>();
    public int ButtonCount { get { return entityButtons.Count; } }

    // Prefabs
    public GameObject projectilePrefab;

    [HideInInspector]
    public List<MeshRenderer> childMeshRender;

    // Hidden public
	[HideInInspector]
	public AudioSource audioSource;
    [HideInInspector]
    public bool UseMovementDriver = true;
    [HideInInspector]
    public int Health = 0;
    [HideInInspector]
    public Vector3 WeaponLocation { get { return entityWeapon.transform.position; } }
    public Quaternion WeaponRotation { get { return Quaternion.Euler(new Vector3(0, entityWeapon.transform.rotation.y, 0)); } }
	[HideInInspector]
	public Mover mover;

    // Public settings, with custom operators
    public Vector3 RallyPoint
    {
        get
        {
            Vector3 root = rootRallyPoint;
            root.y = 0;

            // Pick a random point within a 2 unit sphere, this will serve as a random "Offset" to the true rally point.
            Vector2 uC = Random.insideUnitCircle * GlobalConfiguration.RallyPointOffsetRadius;

            return root + new Vector3(uC.x, 0, uC.y);
        }
    }

    [HideInInspector]
    public Vector3 rootRallyPoint = Vector3.zero;

    // Private settings
    private Entity entity;

    // Utility stuff
    public void RecieveParent(Entity parent) { 
        // Store a link to our parent Entity, for the couple sparce things that need a setup function this is a safe place for it
        entity = parent;
    
        // We'll need a default rally point for anything really, because we can't actually predict where it'll be
        Vector3 rallyOffset = new Vector3(4, 0, 0);
        rootRallyPoint = entity.transform.position + rallyOffset;

		//Debug.Log ("Entity setup... Building(" + IsBuilding.ToString () + ") Local(" + entity.locallyOwned.ToString () + ")");

        

        if (entity.locallyOwned)
        {

            // Setup sound
            if (!audioSource)
            {
                audioSource = gameObject.AddComponent<AudioSource>();

                if (spawnSound)
                {
                    audioSource.PlayOneShot(spawnSound);
                }
            }

            if (IsBuilding == false)
            {

                //Debug.Log ("Spawning mover");

                GameObject g = Instantiate(Resources.Load("EntityMover", typeof(GameObject))) as GameObject;
                mover = g.GetComponent<Mover>();

                mover.Setup(entity);

                UseMovementDriver = true;
            }
            else
            {
                UseMovementDriver = false;
            }

            // Hide then show, to properlly prepare flags and such
        }
        else
        {
            // Hide the range interface
        }
    }

	// The "MoveTo" que
	private Vector3 lastQue;
	private bool hasQue = false;

	public void MoveTo(Vector3 target)
	{
		if (mover) {
			mover.SetDestination (target);
		} else {
			// Store in que
			hasQue = true;
			lastQue = target;
		}
	}

	public void UpdateMoveQue()
	{
		if (hasQue) {
			// Check for mover again
			if (mover)
			{
				// We can now push the que
				hasQue = false;
				mover.SetDestination(lastQue);
			}
		}
	}

    void Awake()
    {
        // Link up our nav mesh agent.

        // Finall all the mesh render's
        MeshRenderer[] r = GetComponentsInChildren<MeshRenderer>();

        foreach(MeshRenderer m in r)
        {
            RangeInterface rInterface = m.gameObject.GetComponent<RangeInterface>();

            if (!rInterface)
            {
                //m.enabled = false; // Hide these to begin with
                childMeshRender.Add(m); // Safe to add
            }
            else
            {
                // Destroy if local
                m.enabled = false;
            }
        }
    }
}

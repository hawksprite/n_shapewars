﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SoundManager : MonoBehaviour {

    private static SoundManager _instance;
    public static SoundManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<SoundManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    private AudioSource audioSource;

    public AudioClip[] backgroundMusicLibrary;
    private int lastLibraryChoice = 0;

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);

            // Linkup son
            audioSource = GetComponent<AudioSource>(); // Find and configure our audio source
            audioSource.loop = false;
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    void Update()
    {
        // Manage our background music
        updateBackgroundMusic();

        // This happens to be the best place to check for screenshots
        if (Input.GetKeyDown(KeyCode.F12))
        {
            string fileName = generateSafeName();
            Application.CaptureScreenshot(fileName);

            Debug.Log("Saved a screenshot at " + fileName);
        }
    }

    int checkDelay = 10; // We want an inital check
    void updateBackgroundMusic()
    {
        // Only if we have background music
        if (backgroundMusicLibrary.Length > 0)
        {
            // Always increment
            checkDelay++;

            // Keep our sound up to date
            audioSource.volume = GlobalConfiguration.Volume_BackgroundMusic;

            // See if we're playing
            if (audioSource.isPlaying == false)
            {
                // Only assign once the delay is over 10
                if (checkDelay >= 10)
                {
                    // Pick a random clip to play
                    int choice = Random.Range(0, backgroundMusicLibrary.Length);

                    audioSource.clip = backgroundMusicLibrary[choice];
                    audioSource.Play();

                    // Store it
                    lastLibraryChoice = choice;

                    // Reset our check delay
                    checkDelay = 0;
                }
            }
        }
    }


    string generateSafeName()
    {
        string baseName = Application.dataPath + Path.DirectorySeparatorChar + "ShapeWars_Screenshot_";

        int worker = 1;

        while (File.Exists(baseName + worker.ToString() + ".png")) { worker++; } // This will find a name that doesn't exist

        return baseName += worker.ToString() + ".png"; // Return, and append png
    }
 
}

﻿using UnityEngine;
using System.Collections;

public class PlayClip : MonoBehaviour {

	public AudioClip clip;

	private AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
		audioSource.PlayOneShot (clip);
	}
}

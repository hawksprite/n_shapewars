﻿using UnityEngine;
using System.Collections;

public class ParticleManager : MonoBehaviour {

    ParticleManagerSync _pms;

    //Here is a private reference only this class can access
    private static ParticleManager _instance;

    //This is the public reference that other classes will use
    public static ParticleManager instance
    {
        get
        {
            //If _instance hasn't been set yet, we grab it from the scene!
            //This will only happen the first time this reference is used.
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<ParticleManager>();
            return _instance;
        }
    }

    void Start()
    {
        GameObject g = PhotonNetwork.Instantiate("ParticleManagerSync", Vector3.zero, Quaternion.identity, 0);
        _pms = g.GetComponent<ParticleManagerSync>();
    }

    public GameObject[] particleLibrary;

    public void PlayParticle(int id, Vector3 location)
    {
        // Send out an RPC to play a given particle, then spawn it locally.

        // Old instnation
        //PhotonNetwork.Instantiate(particleLibrary[id].name, location, Quaternion.identity, 0);

        _pms.SendParticle(id, location);

        // Now play it locally
        PlayParticleLocal(id, location);
    }

    public void PlayParticleLocal(int id, Vector3 location)
    {
        GameObject.Instantiate(particleLibrary[id], location, Quaternion.identity);
    }
}

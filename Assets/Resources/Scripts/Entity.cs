﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity : MonoBehaviour {

	public enum States { Idle, Moving, Agro }
	public enum Side { Friendly, Enemy }
    public enum AgroType { Ground, Sky, Both }

    // Links
    private EntitySync entitySync;

    [HideInInspector]
    public bool isSelected = false;
    private bool lastFrameSelected = false;

    // Hidden vars
    [HideInInspector]
    public EntitySettings settings;
    [HideInInspector]
	public States currentState;
    [HideInInspector]
	public States StartingState = States.Idle;
    [HideInInspector]
	public Side currentSide = Side.Enemy;
    [HideInInspector]
    public bool locallyOwned = false;
    [HideInInspector]
    public Color initialColor;

    // Misc
    [HideInInspector]
    public Vector3 targetLocation = new Vector3(-1, -1, -1);
	private float targetRotation = 0.0f;
	private Entity currentTarget;

    // Selection object stuff
    private RangeInterface rangeInterface;

    public void InitalizeColor(Color c)
    {
        // Initalize our color stuff 
        initialColor = c;
        settings.mainRenderer.material.color = initialColor; // Apply it to our mainRender
    }

    public void Initialize(bool local, bool friendly)
    {
		locallyOwned = local;

        // Find all of our script refrences in awake.
        settings = GetComponent<EntitySettings>(); settings.RecieveParent(this);
        entitySync = GetComponent<EntitySync>();
        rangeInterface = GetComponentInChildren<RangeInterface>();

        rangeInterface.gameObject.tag = "RANGE";
		
        if (friendly) { currentSide = Side.Friendly; } else { currentSide = Side.Enemy; }
        
        if (targetLocation == new Vector3(-1, -1, -1))
        {
            targetLocation = transform.position;
        }

        currentState = StartingState;
        settings.Health = settings.MaxHealth;

        // Initialize the range interface
        rangeInterface.SetDisplay(false);
        rangeInterface.SetRange(settings.AttackRange);

        if (!locallyOwned)
        {
            GameObject.Destroy(rangeInterface);
        }

        // If it's local set our own color
        if (local) { InitalizeColor( PlayerStats.Player.Color);}

        // Set up a map marker for this entity.
        setupMapMarker();

        OPT_Hide();

        // If it's locally owned show now. Otherwise it'll wait for the scrip tinitalization.
        if (locallyOwned)
        {
            OPT_Show(); // If it's local show it.

            // Manually do it as well
            //foreach (MeshRenderer mr in settings.childMeshRender) { mr.enabled = true; }
        }

        hasInitialized = true;
    }
    // Utility function for preparing our map marker
    void setupMapMarker()
    {
        // Just spawn it in, then call the setup marker function from it's components.  It will handle the rest.
        GameObject g = (GameObject)Instantiate(Resources.Load("EntityMarker"));
        MarkerSetup m = g.GetComponent<MarkerSetup>();

        m.SetupMarker(this);
    }

    // Used for optimization, to hide and show units based on difference.
    private bool currentlyHidden = false;

    public void OPT_Hide()
    {
        if (currentlyHidden == false)
        {
            currentlyHidden = true;

            // Hide it
            foreach (MeshRenderer r in settings.childMeshRender)
            {
                if (r.gameObject.tag != "RANGE")
                {
                    r.enabled = false;
                }
            }
        }
    }

    public void OPT_Show()
    {
        if (currentlyHidden)
        {
            currentlyHidden = false;

            // Show it
            foreach (MeshRenderer r in settings.childMeshRender)
            {
                if (r.gameObject.tag != "RANGE")
                {
                    r.enabled = true;
                }
            }
        }
    }


    private bool hasInitialized = false;
	// Update is called once per frame
	void Update () {

        if (hasInitialized)
        {
            if (settings.SampleNavMesh)
            {
                targetLocation.y = 0; // Keep sampling the target mesh to make sure our target Y is locked on it
            }

            // Check distance to the camera, if it's above 70, then disable mesh render.
            if (Vector3.Distance(transform.position, PlayerStats.Player.CameraLocation) > GlobalConfiguration.ObjectViewDistance)
            {
                OPT_Hide();
            }
            else
            {
                OPT_Show();
            }

            if (locallyOwned)
            {
				if (settings.UseMovementDriver)
				{
					targetLocation = settings.mover.transform.position;

					if (Vector3.Distance(targetLocation, transform.position) < 0.1f)
					{
                        // Snap to target
                        settings.mover.transform.position = targetLocation;
						transform.position = targetLocation;
					}

					Vector3 targetPointChange = targetLocation - transform.position;

					if (currentState != States.Idle){

                        if (Vector3.Distance(targetLocation, transform.position) > 0.2f)
                        {
                            transform.rotation = Quaternion.LookRotation(targetPointChange.normalized);
                            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + settings.RotationOffset.y, 0);
                        }
					}

					// Now we'll want to actually move our way towards the target
					Vector3 r = Vector3.MoveTowards(transform.position, targetLocation, settings.MovementSpeed * Time.smoothDeltaTime);
					r.y = 0;

					// Zero out
					transform.position = r;
				}

                // Only update local control AI if it's friendly
                if (currentSide == Side.Friendly)
                {
                    // Update our states
                    switch (currentState)
                    {
                        case States.Idle:
                            currentState = idleUpdate();
                            break;
                        case States.Moving:
                            currentState = movingUpdate();
                            break;
                        case States.Agro:
                            currentState = agroUpdate();
                            break;
                    }
                }

                // Do an out of state global check for health to hit 0, then kill our object
                if (settings.Health <= 0)
                {
                    ParticleManager.instance.PlayParticle(0, transform.position);

                    PhotonNetwork.Destroy(this.gameObject);
                }
            }
        }
	}

    void LateUpdate()
    {
        if (locallyOwned)
        {
            // Check there selection stuff
            if (!lastFrameSelected && !isSelected)
            {
                // Hide the range
                //rangeInterface.SetDisplay(false);
                SetSelectionEffect(false);
            }
            else
            {
                // Show the range
                //rangeInterface.SetDisplay(true);
                SetSelectionEffect(true);
            }

            lastFrameSelected = isSelected;
        }
        else
        {
            //SetSelectionEffect(false);
        }

        //isSelected = false; // Very last thing set to false for next frame.
    }

    public void FireWeapon(Entity ProjectileTarget)
    {
        // This is called back up from an attached weapon, we should handle it in a seperat script to standardize the RPC call and creation

        Vector3 fireingTarget = ProjectileTarget.transform.position; // We'll want this for the RPC call

        // Create our buffer and send the call, then create it locally this should be closest to syncing on both screens.

        string buffer = NetworkTools.Vector3ToString(fireingTarget); // [Name]|1,1,1 <- Projectile name, projectile target location encapsulated like so

        // TODO: MAKE RPC CALL
        entitySync.NetworkBroadcastWeapon(buffer);

        // Spawn the projectile now
        GameObject g = (GameObject)GameObject.Instantiate(settings.projectilePrefab, settings.WeaponLocation, Quaternion.identity);
        Projectile p = g.GetComponent<Projectile>();

        // Run the projectile setup for a local creation
        p.Setup(true, ProjectileTarget.transform.position, settings.AttackDamage, settings.AttackRange);
    }

    public void EmulateFireWeapon(string emulationData)
    {
        // We'll need to find the prefab and target they're specifying
        Vector3 targetLocation = NetworkTools.Vector3FromString(emulationData);

        // Emulate fireing our weapon to the given location were a target should have existed
        GameObject g = (GameObject)GameObject.Instantiate(settings.projectilePrefab, settings.WeaponLocation, settings.WeaponRotation);
        Projectile p = g.GetComponent<Projectile>();

		p.Setup(false, targetLocation, settings.AttackDamage, settings.AttackRange);
    }

	// Interface type stuff
	public void MoveTo(Vector3 target, bool fromUser = false)
	{
        // Always target to Y location 0, sampling the nav mesh always determines the height
        target.y = 0;

        // Always make the rally point the last requested target from user, if it's a unit the rally point won't much matter
        if (fromUser)
        {
            settings.rootRallyPoint = target;
        }

        // Only apply if we aren't a building
        if (!settings.IsBuilding)
        {
            // Check to see if this uses a driver
            if (settings.UseMovementDriver)
            {
                //settings.entityAgent.SetDestination(target);
				settings.MoveTo(target);
            }
            else
            {
                targetLocation = target;
            }

            // This sets our state to moving as well
            currentState = States.Moving;

            // Only check on from user if it's local
            if (locallyOwned)
            {
                if (fromUser)
                {
                    // Notify movement
                    SendNotification("move");
                }
            }

        }
	}

    public void SendNotification(string message)
    {
        if (settings.SendNotifications)
        {
            gameObject.SendMessage("OnUserAction", message);
        }
    }

    // DEPRECIATED, We now use range object indicators to show what's selected
	public void setOverlayColor(Color overlay)
	{
        settings.mainRenderer.material.SetColor("_Color", overlay);
	}

    public void SetSelectionEffect(bool show)
    {
        // Pass it into our range indicator
        if (rangeInterface)
        {
            rangeInterface.SetDisplay(show);
        }
    }

    public void DisposeSelectionEffect()
    {
        if (rangeInterface)
        {
            GameObject.Destroy(rangeInterface.gameObject);
        }
    }

	public void TakeDamage(int damage)
	{
		settings.Health -= damage;

		// Don't let it go above max health
		if (settings.Health > settings.MaxHealth) {
			settings.Health = settings.MaxHealth;
		}
	}

	public void OnAction(string action)
	{
		// When an action is to be performed on this entity
        // Since this is root entity level this is pretty much the universal actions any entity can recieve.
		if (action == "stop" || action == "halt") {
            if (!settings.UseMovementDriver)
            {
                targetLocation = transform.position;
                currentState = States.Idle;
            }
            else
            {
				settings.MoveTo(transform.position);
            }
		}
	}

	void OnTriggerEnter(Collider hit)
	{
		// If this is our local tank check for the collision
		Projectile projectile = hit.GetComponent<Projectile>();

		if (projectile)
		{
			// If it was an projectile

            // At this point we need to handle things in 2 ways
            // If locally owned entity we'll assume it's an emulated enemy projectile

            if (currentSide == Side.Friendly && projectile.friendly)
            {
                // In this case we have friendly projectile's hitting friendly entitys.

                // Don't really "Register" this collision
            }
            else
            {
                // Otherwise signal the projectile that hit us to EXPLODE
                projectile.HitObject(this);
            }
		}
	}

	//////////////////////////////////////////////////////////////////
	// AI State update loops
	//////////////////////////////////////////////////////////////////
	States idleUpdate()
	{
		// Need to check for enemy's to agro on
        if (settings.entityWeapon)
        {
            //settings.entityWeapon.SendMessage("ClearTarget");
        }
		
		// Before we go on the hunt, see if we have an active selection within range, if so immediatelly kick back to agro
		if (currentTarget) {
			float currentDistance = Vector3.Distance(currentTarget.transform.position, transform.position);

			if (currentDistance < settings.AttackRange)
			{
				return States.Agro;
			}
		}


        // Hunt for an enemy automatically.
        if (settings.AutoAgro)
        {
            List<Entity> enemyList = EntityManager.getEnemyList();

            if (enemyList.Count >= 1)
            {
                // As long as there's atleast 1 enemy check them

                Entity closestEntity = enemyList[0];
                float closestEntityDistance = Vector3.Distance(transform.position, enemyList[0].transform.position);

                for (int i = 0; i < enemyList.Count; i++)
                {
                    float eD = Vector3.Distance(transform.position, enemyList[i].transform.position);

                    if (!closestEntity)
                    {
                        closestEntity = enemyList[i];
                        closestEntityDistance = eD;
                    }
                    else
                    {
                        if (eD < closestEntityDistance)
                        {
                            closestEntityDistance = eD;
                            closestEntity = enemyList[i];
                        }
                    }
                }

                // See if the closest is within our minnimum agro range
                if (closestEntityDistance <= settings.AttackRange)
                {
                    // Store the target
                    currentTarget = closestEntity;

                    // At this point we have a succesful target so go ahead and kick into agro
                    return States.Agro;
                }

            }
        }

		return States.Idle;
	}

	int frameDelay = 0;
	States movingUpdate()
	{
		// Clear out our weapon rotation
        if (settings.UseMovementDriver)
        {
            // Check the destination distance on our agent, this will determine if we should go back into idle
			frameDelay++;

			if (frameDelay >= 30){
	            if (settings.mover.agent.remainingDistance <= 0.1f)
	            {
					frameDelay = 0;
	                return States.Idle;
	            }
			}

            return States.Moving; // Keep moving
        }
        else
        {
            // Determine the distance to our target
            float targetDistance = Vector3.Distance(targetLocation, transform.position);

            if (targetDistance <= 0.1f)
            {
                // We're close enough!
                return States.Idle; // Return idle state
            }
            else
            {
                // Keep moving towards our target

                // Rotate towards the target
                Vector3 targetPointChange = targetLocation - transform.position;

                if (settings.RotateOnMovement)
                {
                    transform.rotation = Quaternion.LookRotation(targetPointChange.normalized);
                    transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + 90, 0);
                }

                // Now we'll want to actually move our way towards the target
                transform.position = Vector3.MoveTowards(transform.position, targetLocation, settings.MovementSpeed * Time.smoothDeltaTime);
            }

            return States.Moving;
        }
	}

	States agroUpdate()
	{
		if (!currentTarget) {
			// If our target has died on us go back to idle
			return States.Idle;
		} else {
			// While it might not be dead, make sure it's still in range
			float range = Vector3.Distance(transform.position, currentTarget.transform.position);

			if (!(range < settings.AttackRange))
			{
				return States.Idle;
			}
		}

		// Update our weapon AI
        if (settings.entityWeapon)
        {
			if (settings.AutoWeapon){
            	settings.entityWeapon.SendMessage("UpdateWeapon", currentTarget);
			}
        }

		return States.Agro;
	}

}

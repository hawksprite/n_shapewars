﻿using UnityEngine;
using System.Collections;

public class info_CloseInfo : MonoBehaviour {

    public GameObject infoRoot;

    void OnClick()
    {
        // Hide the info root
        infoRoot.SetActive(false);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ServerWidgetManager : MonoBehaviour {

    public GameObject gameEntryPrefab;
    public UILabel masterStatus;

    public List<GameEntryController> entryCache = new List<GameEntryController>();

    public float HeightPerRow = 80.0f;
    public Vector2 initialRowPosition = new Vector2(0, -40);

    void CheckForDeadList(RoomInfo[] current)
    {
        List<GameEntryController> newList = new List<GameEntryController>();

        for (int i = 0; i < entryCache.Count; i++)
        {
            // Go through each item, only add it to newList if it's found in our current set
            bool found = false;

            foreach (RoomInfo r in current)
            {
                if (entryCache[i].RoomName == r.name)
                {
                    found = true;
                }
            }

            if (found)
            {
                newList.Add(entryCache[i]);
            }
            else
            {
                // Destroy it
                GameObject.Destroy(entryCache[i].gameObject);
            }
        }

        // Re apply our list
        entryCache = newList;
    }

    void VerifyListIntegrity()
    {
        // At the head make sure there aren't any dead list entries
        List<GameEntryController> newList = new List<GameEntryController>();

        foreach (GameEntryController g in entryCache)
        {
            if (g)
            {
                newList.Add(g); // If it's still good save it.
            }
        }

        // Now re-apply the list, effectively dumping bad entries
        entryCache = newList;
    }

    void SetEntryData(RoomInfo newData)
    {
        bool found = false;

        for (int i = 0; i < entryCache.Count; i++)
        {
            if (entryCache[i].RoomName == newData.name)
            {
                // This is it, update it's data
                found = true;
                entryCache[i].SetData(newData.name, newData.playerCount, newData.maxPlayers, initialRowPosition - new Vector2(0, i * HeightPerRow), this);
            }
        }

        if (found == false)
        {
            // We need to make a new one!
            GameObject g = GameObject.Instantiate(gameEntryPrefab) as GameObject;
            GameEntryController c = g.GetComponent<GameEntryController>();

            g.transform.parent = transform; // Parent it to this script's object.
            g.transform.localScale = new Vector3(1, 1, 1); // Reset scale

            // Initialize, set it's starting data then add it to our cache
            c.SetData(newData.name, newData.playerCount, newData.maxPlayers, initialRowPosition - new Vector2(0, entryCache.Count * HeightPerRow), this);

            entryCache.Add(c);
        }
    }
	
	void Update () {

        VerifyListIntegrity();

        // Make sure we have a proper entry for each room
        RoomInfo[] currentRooms = PhotonNetwork.GetRoomList();

        // Update our room count here
        masterStatus.text = "There is currently " + currentRooms.Length + " joinable game(s).";

        // NOTE: The Game Entry's entry's are 80 pixel high.
        foreach (RoomInfo r in currentRooms)
        {
            // Update it's data
            SetEntryData(r);
        }

        // Make sure we don't have any "Dead" entriest
        CheckForDeadList(currentRooms);

        VerifyListIntegrity();
	}
}

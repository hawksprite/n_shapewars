﻿using UnityEngine;
using System.Collections;

public class GameEntryController : MonoBehaviour {

    public UILabel roomCountLabel;
    public UILabel roomNameLabel;
    public GameEntryJoin roomJoinButton;
    private UIAnchor uiAnchor;

    [HideInInspector]
    public string RoomName;

   public void SetPixelOffset(Vector2 loc)
    {
       uiAnchor.pixelOffset = loc;
    }

    public void SetData(string name, int count, int max, Vector2 loc, ServerWidgetManager parent)
    {
        if (!uiAnchor)
        {
            uiAnchor = GetComponent<UIAnchor>();
            uiAnchor.runOnlyOnce = false;
            uiAnchor.container = parent.gameObject;
        }

        roomJoinButton.RoomName = name;
        roomCountLabel.text = count.ToString() + " / " + max.ToString();
        roomNameLabel.text = name;
        RoomName = name;
        SetPixelOffset(loc);
    }
	
}

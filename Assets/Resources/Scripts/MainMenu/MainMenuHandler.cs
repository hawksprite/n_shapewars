﻿using UnityEngine;
using System.Collections;

public class MainMenuHandler : MonoBehaviour {

	string username = "";
	
	public UIInput nameInput;

    public bool QuickStart = true;

    // Hooks for managing the main menu
    public GameObject mainMenuRoot;
    public GameObject connectingRoot;
    public GameObject settingsRoot;
    public GameObject gamesRoot;
    public GameObject helpRoot;
	public GameObject infoRoot;
	public GameObject creditsRoot;
	public GameObject storyRoot;

	public void Awake()
	{
		// Our main menu is going to setup photon
		PhotonNetwork.automaticallySyncScene = true;
        //PhotonNetwork.autoCleanUpPlayerObjects = false;
        //PhotonNetwork.MaxResendsBeforeDisconnect = 10;

		// Check our connection and attempt a connection if needed
		if (PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated)
		{
			PhotonNetwork.ConnectUsingSettings(GlobalConfiguration.ClientVersionIdentifier);
		}
	}

	void Start()
	{
		// Randomize the number after user
		username = "Player(" + Random.Range (1, 9999) + ")";

		nameInput.value = username;

		PhotonNetwork.playerName = username;

        settingsRoot.SetActive(false);
	}

	// Update is called once per frame
    bool _t = false;
    int _tc = 0;
	void Update () {
        // 2 Main over arching states, connected to Photon and not.
        if (!PhotonNetwork.connected)
        {
            // Hide everything and show the "Connecting..." message
            __setConnecting();
        }
        else
        {
            __setConnected();

            // Now in our update loop from this point forward we can always safely assume that we're connected to Photon.



            // Update our menu normally
            if (QuickStart)
            {
                if (_t == false)
                {
                    _tc++;
                    if (_tc >= 60)
                    {
                        _t = true;

                        ButtonPressed("create");
                    }
                }
            }
        }
	}

    // 2 Shim functions that can just measilly keep getting hit up for connecting to not connecting
    private bool __hasHidden = false;
    private bool __hasShown = false;
    void __setConnecting()
    {
        if (__hasHidden == false)
        {
            // Hide the root
            __hasHidden = true;
            connectingRoot.SetActive(true);
            mainMenuRoot.SetActive(false);
        }
    }

    void __setConnected()
    {
        if (__hasShown == false)
        {
            // Show the root
            __hasShown = true;
            mainMenuRoot.SetActive(true);
            connectingRoot.SetActive(false);
            gamesRoot.SetActive(false);
        }
    }

    public void ShowHelp()
    {
		infoRoot.SetActive (false);
		helpRoot.SetActive (true);
    }

    public void HideHelp()
    {
        helpRoot.SetActive(false);
		infoRoot.SetActive (true);
    }

    public void ShowGames()
    {
        mainMenuRoot.SetActive(false);
        gamesRoot.SetActive(true);
    }

    public void HideGames()
    {
        mainMenuRoot.SetActive(true);
        gamesRoot.SetActive(false);
    }

    public void ShowSettings()
    {
        settingsRoot.SetActive(true);
        mainMenuRoot.SetActive(false);
    }

    public void CloseSettings()
    {
        settingsRoot.SetActive(false);
        mainMenuRoot.SetActive(true);
    }

	public void ButtonPressed(string name)
	{
		// At the start refresh our gathered variables so our input's are up to date
		username = nameInput.value;
		PhotonNetwork.playerName = username;

        if (name == "close-games")
        {
            HideGames();
        }

        if (name == "open-games")
        {
            ShowGames();
        }

        if (name == "open-help")
        {
            ShowHelp();
        }

        if (name == "close-help")
        {
            HideHelp();
        }

        if (name == "close-settings")
        {
            CloseSettings();
        }

		if (name == "open-info") {
			infoRoot.SetActive(true);
			mainMenuRoot.SetActive(false);
		}

		if (name == "close-info") {
			infoRoot.SetActive(false);
			mainMenuRoot.SetActive(true);
		}

        if (name == "open-settings")
        {
            ShowSettings();
        }

		if (name == "open-credits") {
			creditsRoot.SetActive(true);
			infoRoot.SetActive(false);
				}

		if (name == "close-credits") {
			creditsRoot.SetActive(false);
			infoRoot.SetActive(true);
				}

		if (name == "open-story") {
			infoRoot.SetActive(false);
			storyRoot.SetActive(true);
				}

		if (name == "close-story") {
			infoRoot.SetActive(true);
			storyRoot.SetActive(false);
				}

		if (name == "create") {
            /*
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.isOpen = true;
            roomOptions.isVisible = true;
            roomOptions.maxPlayers = 4;

            PhotonNetwork.JoinOrCreateRoom(username + " - Game", roomOptions, TypedLobby.Default);
             * */

            PhotonNetwork.CreateRoom(username + "'s Game", true, true, 4); // Max of 4 players
		}
	}

	public void OnCreatedRoom()
	{
		PhotonNetwork.LoadLevel (2); // Head over to the lobby
	}
}

﻿using UnityEngine;
using System.Collections;

public class ExitButtonMainMenu : MonoBehaviour {

	void OnClick()
    {
        // Call to exit the game
        LevelManager.instance.ExitGame();
    }
}

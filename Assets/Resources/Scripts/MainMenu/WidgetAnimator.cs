﻿using UnityEngine;
using System.Collections;

public class WidgetAnimator : MonoBehaviour {

	private UIWidget widget;
	private UIPanel panel;
	private TweenAlpha tween;
	private bool isPrep = false;
	
	// Use this for initialization
	void Clean()
	{
		if (tween) {
			Destroy (tween);
		}

		isPrep = false;

		widget = GetComponent<UIWidget> ();
		panel = GetComponent<UIPanel> ();

		if (widget || panel) {
			isPrep = true;

			if (widget){ widget.alpha = 0.0f; }
			if (panel) { panel.alpha = 0.0f; }

			tween = gameObject.AddComponent<TweenAlpha>();
            tween.tweenFactor *= 1.40f; // Up the speed from stock by 40%
			tween.from = 0.0f;
			tween.to = 1.0f;
		}
	}

	// On enable reset the tween
	void OnEnable()
	{
		Clean ();
	}
}

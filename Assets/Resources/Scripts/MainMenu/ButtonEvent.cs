﻿using UnityEngine;
using System.Collections;

public class ButtonEvent : MonoBehaviour {

	public string ButtonName = "default";
	private MainMenuHandler mainMenuHandler;

	void Start()
	{
		// Find and link our handler
		MainMenuHandler handler = FindObjectOfType (typeof(MainMenuHandler)) as MainMenuHandler;

		mainMenuHandler = handler;
	}

	void OnClick()
	{
		// Call the event
		mainMenuHandler.ButtonPressed (ButtonName);
	}
}

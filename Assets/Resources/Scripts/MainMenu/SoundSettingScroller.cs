﻿using UnityEngine;
using System.Collections;

public class SoundSettingScroller : MonoBehaviour {

	UISlider slider;

	// Use this for initialization
	void Start () {
		slider = GetComponent<UISlider>();

		slider.value = GlobalConfiguration.Volume_BackgroundMusic;
	}
	
	// Update is called once per frame
	void Update () {
		GlobalConfiguration.Volume_BackgroundMusic = slider.value;
	}
}

﻿using UnityEngine;
using System.Collections;

public class mainMenu_VersionScript : MonoBehaviour {

    UILabel uiLabel;

	void Awake(){
        uiLabel = GetComponent<UILabel>();
        uiLabel.text = "Version ( " + GlobalConfiguration.ClientVersionIdentifier + " )";
    }
}

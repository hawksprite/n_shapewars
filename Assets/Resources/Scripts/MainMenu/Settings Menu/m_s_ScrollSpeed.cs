﻿using UnityEngine;
using System.Collections;

public class m_s_ScrollSpeed : MonoBehaviour {

    UISlider slider;

    

	void Awake()
    {
        slider = GetComponent<UISlider>();

        slider.value = GlobalConfiguration.ConvertToFloatRange(GlobalConfiguration.MaxCameraMoveSpeed, GlobalConfiguration.CameraMoveSpeed);
    }

    float previousV = 0.0f;
    void Update()
    {
        if (slider.value != previousV)
        {
            previousV = slider.value;
            GlobalConfiguration.CameraMoveSpeed = GlobalConfiguration.ConvertFromFloatRange(GlobalConfiguration.MaxCameraMoveSpeed, slider.value);

            //Debug.Log("Set CameraMoveSpeed to " + slider.value.ToString());
        }
    }
}

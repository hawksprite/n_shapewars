﻿using UnityEngine;
using System.Collections;

public class m_s_PostProcessing : MonoBehaviour {

    UIToggle slider;

    EventDelegate eDelegate = new EventDelegate();

    void Awake()
    {
        slider = GetComponent<UIToggle>();


        slider.value = GlobalConfiguration.EnabledPostProcesses;
    }

    void Update()
    {
        if (slider.value != GlobalConfiguration.EnabledPostProcesses)
        {
            GlobalConfiguration.EnabledPostProcesses = slider.value;

            //Debug.Log("Set EnablePostProcesses to" + slider.value.ToString());
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class FactoryScript : MonoBehaviour {

	public bool isMine
	{
		get{
			if (parentEntity){
				return parentEntity.locallyOwned;
			}
			else{
				return false;
			}
		}
	}
	private Entity parentEntity;

	// Use this for initialization
	void Start () {
		parentEntity = GetComponent<Entity> ();
	}

	System.DateTime lastSpawned;
	bool initialSpawn = false;

	// Update is called once per frame
	void Update () {
		if (isMine) {

		}
	}

	void OnAction(string action)
	{
        if (isMine)
        {
            if (action == "build-infantry")
            {
                GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CUBE_Infantry, transform.position, Quaternion.identity, this.parentEntity);
            }

            if (action == "build-flack")
            {
                GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CUBE_Flackjack, transform.position, Quaternion.identity, this.parentEntity);
            }

            if (action == "build-hammer")
            {
                GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CUBE_Hammerhead, transform.position, Quaternion.identity, this.parentEntity);
            }

            if (action == "build-razorbacks")
            {
                GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CUBE_Razorback, transform.position, Quaternion.identity, this.parentEntity);
            }

            if (action == "build-rifle")
            {
                GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CUBE_Rifleman, transform.position, Quaternion.identity, this.parentEntity);
            }

            if (action == "build-gernade")
            {
                GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CUBE_Gernader, transform.position, Quaternion.identity, this.parentEntity);
            }

            if (action == "build-raven")
            {
                GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CUBE_RavenLight, transform.position, Quaternion.identity, this.parentEntity);
            }
        }
	}
}

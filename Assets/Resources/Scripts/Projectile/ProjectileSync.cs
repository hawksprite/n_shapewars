﻿using UnityEngine;
using System.Collections;

public class ProjectileSync : Photon.MonoBehaviour {

	private Projectile entity;
	
	void Awake()
	{
		entity = GetComponent<Projectile> ();
		
		if (photonView.isMine)
		{
			//entity.isOurs = true;
			//entity.friendly = true;
		}
		else
		{    
			//entity.isOurs = false;
			//entity.friendly = false;
		}
		
		gameObject.name = gameObject.name + photonView.viewID;
	}
	
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			//We own this player: send the others our data
			stream.SendNext(0);
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation); 
		}
		else
		{
			//Network player, receive data
			int t = (int)stream.ReceiveNext();
			correctEPos = (Vector3)stream.ReceiveNext();
			correctERot = (Quaternion)stream.ReceiveNext();
		}
	}
	
	private Vector3 correctEPos = Vector3.zero; //We lerp towards this
	private Quaternion correctERot = Quaternion.identity; //We lerp towards this
	
	void Update()
	{
		if (!photonView.isMine)
		{
			//Update remote player (smooth this, this looks good, at the cost of some accuracy)
			//transform.position = Vector3.Lerp(transform.position, correctEPos, Time.deltaTime * 5);
			//transform.rotation = Quaternion.Lerp(transform.rotation, correctERot, Time.deltaTime * 5);
			transform.position = correctEPos;
			transform.rotation = correctERot;
		}
	}
}

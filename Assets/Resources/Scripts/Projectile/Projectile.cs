﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	[HideInInspector]
    public Vector3 initialTargetPosition;
	private Vector3 Velocity = Vector3.zero;
	public float moveSpeed = 500;
    private bool targetMode = false;
    public Vector3 AngularOffset = new Vector3(0, 0, 0); // Offset our euler angles by these values.
	public GameObject deathPrefab;

	[HideInInspector]
	public bool friendly = true;
	[HideInInspector]
	public bool isOurs = false;

	[HideInInspector]
    public bool DeathPrefabOnMiss = false;
    public float TargetThreshold = 0.3f;

    [HideInInspector]
    public int ProjectileDamage = 0;

	bool hasSavedInitial = false;
	Vector3 initialLocation;
	float travelDistance;
	Vector3 travelDirection = Vector3.zero;

	public bool UpdatePosition = true;

	private AudioSource audioSource;
	public AudioClip spawnSound;

    private bool isReady = false;
    public void Setup(bool isOurs, Entity target, int damage, float distance)
    {
		Setup (isOurs, target.transform.position, damage, distance);
    }

	void Awake()
	{
		audioSource = GetComponent<AudioSource> ();
	}

    public void Setup(bool isOurs, Vector3 targetLocation, int damage, float distance)
    {
        // Seperate manually called initalization function so we know it has everything before it's first update frame
        this.isOurs = isOurs; // Set the flags correctly
        this.friendly = isOurs;

        initialTargetPosition = targetLocation;

        ProjectileDamage = damage;

		travelDistance = distance + 6;

        // All good!
        isReady = true;

        // GO into target mode
        targetMode = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (isReady)
        {
			if (hasSavedInitial == false){
				hasSavedInitial = true;
				initialLocation = transform.position;

				// Handle spawn sound
				if (audioSource)
				{
					if (spawnSound)
					{
						audioSource.PlayOneShot(spawnSound);
					}
				}

				// Get the direction
				travelDirection = (initialTargetPosition - transform.position).normalized;

				transform.rotation = Quaternion.LookRotation(travelDirection);
				transform.rotation = Quaternion.Euler(AngularOffset.x, transform.rotation.eulerAngles.y + AngularOffset.y, AngularOffset.z);
			}

			if (UpdatePosition){
				transform.position = transform.position + (travelDirection * moveSpeed * Time.smoothDeltaTime);
			}

            // If we get to our target die
			if (Vector3.Distance(initialLocation, transform.position) >= travelDistance)
			{
				KillProjectile(DeathPrefabOnMiss);
			}

        }
	}

	// Call for a hit
	public void HitObject(Entity hitEntity)
	{
        // Skip the whole process if it's enemy on enemy
        if (isOurs == false && hitEntity.currentSide == Entity.Side.Enemy) { }
        else
        {
            if (isOurs)
            {
                // If this was fired in a friendly manor then we don't actually want to register damage.
            }
            else
            {
                // However if this projectile Isn't ours and if the entity it hit is, then an emulated projectile originating from an enemy unit hit us.  So we should register the damage
                if (hitEntity.locallyOwned)
                {
                    // As soon as this is called and a entity takes damage, zero it out to prevent anything else from getting hit by this projectile.
                    hitEntity.TakeDamage((int)ProjectileDamage);
                    ProjectileDamage = 0;
                }
            }

            KillProjectile();
        }
	}

    public void KillProjectile(bool showDeath = true)
    {
        if (showDeath)
        {
            // Doesn't matter who it hit however if it has a death prefab make it look pretty!
            if (deathPrefab)
            {
                GameObject go = GameObject.Instantiate(deathPrefab, transform.position, transform.rotation) as GameObject;// PhotonNetwork.Instantiate(deathPrefab.name, transform.position, transform.rotation,0) as GameObject;
                go.transform.Rotate(90, 0, 0);

                // Since we're not networking these double check that it doesn't ask Photon to destroy the particle later
                CFX_AutoDestructShuriken c = go.GetComponent<CFX_AutoDestructShuriken>();
                if (c)
                {
                    c.isNetworked = false;
                }
            }
        }

        // Kill the projectile
        GameObject.Destroy(this.gameObject);
    }
}

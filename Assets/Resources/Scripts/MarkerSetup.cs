﻿using UnityEngine;
using System.Collections;

public class MarkerSetup : MonoBehaviour {

    private UISprite uiSprite;
    private UIAnchor uiAnchor;
    private GameObject miniMapObject;
    private Entity parentEntity;

    private bool shouldRender = false;

	public void SetupMarker(Entity parent)
    {
        // Keep this link, we'll want one frame to go by for a full safe instanation of this object before we prepare the full marker.
        parentEntity = parent;

        checkSetup();
    }

    bool hasPrepared = false;
    void checkSetup()
    {
        if (hasPrepared == false)
        {
            // Link up everything we'll need to operate and properly get configered
            uiAnchor = GetComponent<UIAnchor>();
            uiSprite = GetComponent<UISprite>();
            miniMapObject = GameObject.FindGameObjectWithTag("MAP");

            // Make sure this marker is completely setup correctly
            gameObject.transform.parent = miniMapObject.transform;

            // Make sure we zero out all the transform stuff after parenting properlly.
            transform.position   = Vector3.zero;
            transform.rotation   = Quaternion.identity;
            transform.localScale = new Vector3(1, 1, 1);

            // Prepare UISprite
            uiSprite.width = 5;
            uiSprite.height = 5;
            uiSprite.color = parentEntity.initialColor; // Use the calling entity's color for the marker

            // Prepare our anchor
            uiAnchor.container = miniMapObject;
            //uiAnchor.uiCamera = GameObject.FindGameObjectWithTag("UI_Camera").GetComponent<Camera>(); <- It seems like the script it self safely finds this camera.
            uiAnchor.pixelOffset = prepareMapLocation(parentEntity.transform.position);

            // We'll want to update pretty often as our units move around.
            uiAnchor.runOnlyOnce = false;

            hasPrepared = true;
        }
    }

    public Vector2 prepareMapLocation(Vector3 world)
    {
        // Prepare our anchor offset data for a given world location
        float pX = world.x * 2;
        float pZ = world.z * 2;

        return new Vector2(pX, pZ);

    }

    bool startDieing = false;
    int dieTimer = 0;

    void Update()
    {
        // Update the color alpha value to render avalibility
        shouldRender = checkIfRender();

        if (shouldRender)
        {
            Color c = uiSprite.color; c.a = 1.0f;
            uiSprite.color = c;
        }
        else
        {
            Color c = uiSprite.color; c.a = 0.0f;
            uiSprite.color = c;
        }

        if (startDieing)
        {
            if (!shouldRender) { GameObject.Destroy(this.gameObject); return; } // Don't waste time on an animation that they won't see

            // When it hit's this point the parent entity died.  Begin our "Death" animation on the map
            dieTimer++;

            // Slowly get larger, work our way to 35 by 35, and go full red
            uiSprite.width += 1;
            uiSprite.height += 1;

            Color b = uiSprite.color; b.r = 1.0f;
            uiSprite.color = b;

            if (dieTimer >= 30)
            {
                GameObject.Destroy(this.gameObject); // Alright this is our end, just go ahead and die
            }
        }
        else
        {
            // If our parentEntity die's then call for the death of this marker
            if (!parentEntity)
            {
                HandleDeath();
            }
            else
            {
                // If it's still alive then update it's location on the map!
                if (shouldRender)
                {
                    uiAnchor.pixelOffset = prepareMapLocation(parentEntity.transform.position);
                }
            }

            // Hide it if we shouldn't be showing this right now
        }
    }

    private bool checkIfRender()
    {
        if (parentEntity.currentSide == Entity.Side.Friendly)
            return true;

        // Else it depends if we should show enemy's
        return GlobalConfiguration.ShowEnemyOnMinimap;
        
    }

    public void HandleDeath()
    {
        startDieing = true;
    }
}

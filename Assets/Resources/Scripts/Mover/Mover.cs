﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {

	[HideInInspector]
	public Entity e;
	[HideInInspector]
	public NavMeshAgent agent;

	private bool hasSetup = false;

	void Awake()
	{
		agent = GetComponent<NavMeshAgent> ();
	}

	// Use this for initialization
	public void Setup(Entity parent)
	{
		e = parent;

		transform.position = parent.transform.position;
		transform.rotation = parent.transform.rotation;

		agent.speed 				= e.settings.MovementSpeed;
		agent.acceleration 			= e.settings.MovementSpeed * 10;
		agent.angularSpeed 			= e.settings.MovementSpeed * 10;
		agent.stoppingDistance      = 0;
		agent.autoBraking           = true;
		agent.radius 				= parent.settings.ObstacleAvoidanceRadius;
		agent.obstacleAvoidanceType = ObstacleAvoidanceType.LowQualityObstacleAvoidance;
	}

	public void SetDestination(Vector3 target)
	{
		agent.SetDestination (target);
	}

}

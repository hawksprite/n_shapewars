﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectionShell {
    public List<GameObject> selectionCache = new List<GameObject>();

    public void Set(List<Entity> source)
    {
        foreach (Entity s in source)
        {
            selectionCache.Add(s.gameObject);
        }
    }

    // Does this cache have anything selected?
    public bool HasSelection
    {
        get
        {
            if (selectionCache.Count == 0)
            {
                return false;
            }

            return true;
        }
    }

    public void VerifySelection()
    {
        if (HasSelection == false)
        {
            return; // Don't bother if nothing is selected
        }

        // Re-build our selection to weed out dropped objects
        List<Entity> entityBuffer = new List<Entity>();

        foreach (GameObject g in selectionCache)
        {
            if (g)
            {
                Entity e = g.GetComponent<Entity>();

                // If it's still an entity
                if (e)
                {
                    // Add it
                    entityBuffer.Add(e);
                }
            }
        }

        // Refresh our list with the one we generated to effectivelly prune the unneded objects
        Set(entityBuffer);
    }

    public List<Entity> Get()
    {
        List<Entity> entityBuffer = new List<Entity>();

        foreach (GameObject g in selectionCache)
        {
            if (g)
            {
                Entity e = g.GetComponent<Entity>();

                // If it's still an entity
                if (e)
                {
                    // Add it
                    entityBuffer.Add(e);
                }
            }
        }

        // Refresh our list with the one we generated to effectivelly prune the unneded objects
        Set(entityBuffer);

        // Return our buffer
        return entityBuffer;
    }

    public void Clear()
    {
        selectionCache.Clear();
    }
}

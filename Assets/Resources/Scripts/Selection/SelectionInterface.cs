﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectionInterface : MonoBehaviour {

    // Make this object a singleton
    private static SelectionInterface _instance;
    // This puppy makes sure that when we're statically called from other scripts that we make sure this isn't a null version of the General Controller.
    public static SelectionInterface instance { get { if (_instance == null) { _instance = GameObject.FindObjectOfType<SelectionInterface>(); } return _instance; } }

	public PlayerSelection playerSelection;

	public Vector3 MouseWorldLocation;

	private string clearSpriteName = "subScheme03";

    // We're using a statically typed array here so that the buttons retain there order based on there index.
    public List<ActionButtonSetup> actionButtonLibrary = new List<ActionButtonSetup>();

    public RaycastHit lastMouseHit;
    public bool mouseOverGUI(Vector3 loc)
    {
        // NOTE: We're outing to the lastMouseHit in a higher scope so other scrips can pull info on there mouseOverGUI call's without needing to cast again.

        // Ray cast to mouse, checking to see if we hit the collision boxe's setup on the GUI
        return UICamera.Raycast(loc, out lastMouseHit); // NGUI's utility should give us a boolean if our mouse is over a GUI element
    }

	// Use this for initialization
	void Start () {
        playerSelection = FindObjectOfType<PlayerSelection>(); // Link up our player selection object
	}

	public void clearSelection()
	{
        // Clear all of our action buttons
		for (int i = 0; i < actionButtonLibrary.Count; i++)
        {
            actionButtonLibrary[i].ClearSelectionImage();
            actionButtonLibrary[i].ClearTooltipText();
        }
	}

	void allowSelection()
	{
		
	}

    public void RegisterButton(ActionButtonSetup incoming)
    {
        // All action buttons register with the selection interface on startup.
        actionButtonLibrary.Add(incoming);

        // As we register them keep sorthing the list
        actionButtonLibrary.Sort((x, y) => x.ActionButtonIndex.CompareTo(y.ActionButtonIndex));

        /*
        actionButtonLibrary.Sort(
            delegate(ActionButtonSetup a1, ActionButtonSetup a2)
            {
                if (a1.ActionButtonIndex > a2.ActionButtonIndex)
                {
                    return 0;
                }
                else { return 1; }
            }
        );*/
    }

    public void AbilityButtonPressed(ActionButtonSetup incoming)
    {
        if (hasSelectedEntity())
        {
            for (int i = 0; i < playerSelection.currentSelection.Count; i++)
            {
                int index = incoming.ActionButtonIndex;

                if (playerSelection.currentSelection[i].settings.ButtonCount > index)
                {
                    playerSelection.currentSelection[i].gameObject.SendMessage("OnAction", playerSelection.currentSelection[i].settings.entityButtons[index].ButtonAction);
                }
            }
        }
    }

    public void SendSelectionAction(string action)
    {
        for (int i = 0; i < playerSelection.currentSelection.Count; i++)
        {
            if (playerSelection.currentSelection[i])
            {
                playerSelection.currentSelection[i].OnAction(action);
            }
        }
    }

    bool hasSelectedEntity()
    {
        return playerSelection.currentSelection.Count > 0;
    }

    void checkInterfaceHotkey(KeyCode key, int actionIndex)
    {
        if (Input.GetKeyDown(key))
        {
            if (playerSelection.currentSelection.Count > 0)
            {
                for (int i = 0; i < playerSelection.currentSelection.Count; i++)
                {
                    if (playerSelection.currentSelection[0].settings.ButtonCount >= actionIndex)
                    {
                        playerSelection.currentSelection[i].gameObject.SendMessage("OnAction", playerSelection.currentSelection[0].settings.entityButtons[actionIndex].ButtonAction);
                    }
                }
            }
        }
    }
	
	// Update is called once per frame
	void Update () {

		// See if we have a selection
		if (playerSelection) {
			if (playerSelection.currentSelection.Count > 0)
			{
				Entity selectedEntity = playerSelection.currentSelection[0]; // Just go with the lead object on all cases

                for (int i = 0; i < selectedEntity.settings.entityButtons.Count; i++)
                {
                    actionButtonLibrary[i].SetSelectionImage( selectedEntity.settings.entityButtons[i].ButtonImage );
                    actionButtonLibrary[i].SetTooltipText(selectedEntity.settings.entityButtons[i].ButtonTooltip);
                }

                // Check for some across the board hotkeys
                if (Input.GetKeyDown(KeyCode.H))
                {
                    SendSelectionAction("halt");
                }

                // 5 Top row action hotkey's
                checkInterfaceHotkey(KeyCode.Z, 0);
                checkInterfaceHotkey(KeyCode.X, 1);
                checkInterfaceHotkey(KeyCode.C, 2);
                checkInterfaceHotkey(KeyCode.V, 3);
                checkInterfaceHotkey(KeyCode.B, 4);
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HealingBurst : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}


	int frameStall = 0;
	// Update is called once per frame
	void FixedUpdate () {

		List<Entity> friendlys = EntityManager.getAllyList ();

		foreach (Entity e in friendlys) {
			float distance = Vector3.Distance(e.transform.position, transform.position);

			if (distance <= 10)
			{
				frameStall++;

				if (frameStall >= 5){
					frameStall = 0;
					e.TakeDamage(-1);
				}
			}
			else{
				frameStall = 0;
			}
		}
	}
}

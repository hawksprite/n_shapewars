﻿using UnityEngine;
using System.Collections;

public class DebugCamera : MonoBehaviour {

    public float moveSpeed = 20.0f;

	void Awake(){

	}

	void Start () {
	
	}
	
	void Update () {
	    if (Input.GetKey(KeyCode.A))
        {
            transform.position = transform.position + new Vector3(-moveSpeed * Time.smoothDeltaTime, 0, 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position = transform.position + new Vector3(moveSpeed * Time.smoothDeltaTime, 0, 0);
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.position = transform.position + new Vector3(0, 0, moveSpeed * Time.smoothDeltaTime);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position = transform.position + new Vector3(0, 0, -moveSpeed * Time.smoothDeltaTime);
        }
	}
}

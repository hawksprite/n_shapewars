﻿using UnityEngine;
using System.Collections;

public class SetRotation : MonoBehaviour {

    public Vector3 Rotation;

    void Start()
    {
        transform.rotation = Quaternion.Euler(Rotation);
    }
	
	void Update () {
        transform.rotation = Quaternion.Euler(Rotation);
	}
}

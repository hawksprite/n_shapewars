﻿using UnityEngine;
using System.Collections;

public class RangeInterface : MonoBehaviour {

    private MeshRenderer meshRender;
    private float storedRange = 1.0f;

    // Just return the enabled state of our mesh render
    public bool IsActive
    {
        get
        {
            if (meshRender)
            {
                return meshRender.enabled;
            }
            else
            {
                return false;
            }
        }
    }

	void Start () {
        meshRender = GetComponent<MeshRenderer>();

        // Once we're linked, hide by default
        SetDisplay(false);
	}

    void Update()
    {
        transform.localScale = new Vector3(storedRange * GlobalConfiguration.EntityRangeOperator, storedRange * GlobalConfiguration.EntityRangeOperator, storedRange * GlobalConfiguration.EntityRangeOperator);
    }

    public void SetPosition(Vector3 offset)
    {
        // Instead of zero'ing out then applying offset just apply the offset straight up
        transform.localPosition = offset;
    }

    public void SetRange(float range)
    {
        // Apply the needed scale for our range
        storedRange = range;
    }

    public void SetDisplay(bool show)
    {
        // Pass the bool into the active value for the mesh render
        if (meshRender)
        {
            meshRender.enabled = show;
        }
    }
}

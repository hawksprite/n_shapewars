﻿using UnityEngine;
using System.Collections;

public class DebugTextScript : MonoBehaviour {

	public GUIText textLink;

    public UILabel nameLabel;
    public UILabel yieldLabel;
    public UILabel resourceLabel;
    public UILabel unitLabel;
    public UILabel latencyLabel;

    public PlayerController playerController;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        //textLink.text = transform.position.ToString() + "\r\nVectories = " + Stats.VectoriteBalance.ToString();

        //nameLabel.text = "[e04545]" + PhotonNetwork.player.name + "[-]";
        unitLabel.text = Stats.ownedUnits.ToString();
        resourceLabel.text = Stats.VectoriteBalance.ToString();
        latencyLabel.text = PlayerStats.Player.Ping.ToString();
        yieldLabel.text = PlayerStats.Player.Yield.ToString() + " +/second";

        /*
        // Some debug stuff we'll eventually need to scrap
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            //transform.position = new Vector3(-70, 14, -60);
            playerController.SetLocation(new Vector3(-70, 14, -60));
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            //transform.position = new Vector3(60, 14, -60);
            playerController.SetLocation(new Vector3(60, 14, -60));
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
           // transform.position = new Vector3(60, 14, 75);
            playerController.SetLocation(new Vector3(60, 14, 75));
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            //transform.position = new Vector3(-70, 14, 75);
            playerController.SetLocation(new Vector3(-70, 14, 75));
        }*/
	}
}

using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float ScrollSpeed = 1.0f;
	public float MoveSpeed
    {
        get
        {
            return GlobalConfiguration.CameraMoveSpeed;
        }
    }

	public float LerpPower = 0.5f;

    public float cameraClamp = 100.0f;

	public Transform target;

	public Vector3 targetLocation;
	private PlayerSelection selection;
	public Vector3 mouseWorld;

	public GameObject spawnPrefab;
	public GameObject worldClickPrefab;
	private bool worldClickPrefabSet = false; // Much faster check for if the prefab was set, since it happens on click

    // Watch over the PostProcess enabled flag
    private bool lastPostProcessEnabled = GlobalConfiguration.EnabledPostProcesses;
    public MonoBehaviour[] postProcessSet;

	// Use this for initialization
	void Start () {
		selection = GetComponent<PlayerSelection> ();
		targetLocation = target.position; // Reset our target to the target's current location

		if (worldClickPrefab) {
			worldClickPrefabSet = true;
		} else {
			worldClickPrefabSet = false;
		}

        // Apply the initial post process state
        foreach(MonoBehaviour m in postProcessSet)
        {
            m.enabled = GlobalConfiguration.EnabledPostProcesses;
        }
	}

    public void SetLocation(Vector3 newTarget)
    {
        // Just ignore the Y request, leave that to the user.
        targetLocation.x = newTarget.x;
        targetLocation.z = newTarget.z;
    }

	public int vectors = 300;

    public void FocusCameraOnUnit()
    {
        if (selection.currentSelection.Count > 0)
        {
            // Has something selected, so follow it with the camera
            Vector3 t = selection.currentSelection[0].transform.position;

            t.y = transform.position.y; // None of this zoom in crap.

            targetLocation = t;

            //targetLocation = t - new Vector3((Screen.width / 2), 0, (Screen.height / 2));
        }
    }
	
	// Update is called once per frame
	void Update () {
		Vector3 velocity = Vector3.zero;

        if (Input.GetKeyDown(KeyCode.T))
        {
            FocusCameraOnUnit();
        }

        // Update velocity based on the platform
        if (Application.platform == RuntimePlatform.Android)
        {
            // On android always focus selected unit
            // FocusCameraOnUnit();
        }
        else
        {
            if (Input.GetKey(KeyCode.W))
            {
                velocity.z += MoveSpeed * Time.smoothDeltaTime;
            }

            if (Input.GetKey(KeyCode.S))
            {
                velocity.z -= MoveSpeed * Time.smoothDeltaTime;
            }

            if (Input.GetKey(KeyCode.D))
            {
                velocity.x += MoveSpeed * Time.smoothDeltaTime;
            }

            if (Input.GetKey(KeyCode.A))
            {
                velocity.x -= MoveSpeed * Time.smoothDeltaTime;
            }
        }


		// Check some hotkeys for our units:
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            // If we have units, check them all and send lethal damage.
            if (selection.currentSelection.Count > 0)
            {
                for (int i = 0; i < selection.currentSelection.Count; i++)
                {
                    if (selection.currentSelection[i])
                    {
                        selection.currentSelection[i].TakeDamage(selection.currentSelection[i].settings.MaxHealth + 1);
                    }
                }
            }
        }
        
		velocity.y += (Input.GetAxis ("Mouse ScrollWheel") * ScrollSpeed * -1);

		targetLocation += velocity;

		targetLocation.y = Mathf.Clamp (targetLocation.y, 8, 22);
		targetLocation.z = Mathf.Clamp (targetLocation.z, -cameraClamp, cameraClamp);
		targetLocation.x = Mathf.Clamp (targetLocation.x, -cameraClamp, cameraClamp);

		target.position = Vector3.Lerp (target.position, targetLocation, LerpPower);

		// Determine of the mouse location in world

        // Platform dependent
        if (Application.platform == RuntimePlatform.Android)
        {
            // Check all touches
            int t = 0;
            while (t < Input.touchCount)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.tag == "Nav")
                    {
                        mouseWorld = hit.point;
                        SelectionInterface.instance.MouseWorldLocation = mouseWorld;
                    }
                }

                t++;
            }
        }
        else
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "Nav")
                {
                    mouseWorld = hit.point;
                    SelectionInterface.instance.MouseWorldLocation = mouseWorld;
                }
            }
        }

		// Check for a click to spawn the world click prefab
		if (Input.GetMouseButtonDown (0) || Input.GetMouseButtonDown (1)) {
			if (worldClickPrefabSet)
			{
                if (SelectionInterface.instance.mouseOverGUI(Input.mousePosition) == false)
                {
                    GameObject go = (GameObject)GameObject.Instantiate(worldClickPrefab, mouseWorld, Quaternion.identity);
                    CFX_AutoDestructShuriken ads = go.GetComponent<CFX_AutoDestructShuriken>();
                    ads.isNetworked = false;
                }
			}
		}

        // Monitor the post processing behavior stuff
        if (GlobalConfiguration.EnabledPostProcesses != lastPostProcessEnabled)
        {
            // Need to update there state
            lastPostProcessEnabled = GlobalConfiguration.EnabledPostProcesses;

            for (int i=  0; i < postProcessSet.Length; i++)
            {
                postProcessSet[i].enabled = GlobalConfiguration.EnabledPostProcesses;
            }
        }


		}
	}
	
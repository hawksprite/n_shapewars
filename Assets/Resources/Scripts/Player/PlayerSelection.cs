﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerSelection : MonoBehaviour {
	
	PlayerController playerController;
	
	public GameObject SelectionPrefab;

	bool selectionSpawned = false;

	public List<Entity> currentSelection = new List<Entity> ();

    public int MaxSelectionHistory = 3;

	public UILabel selectionLabel;

	public Camera nguiCam;

    public int mouseDragThreshold = 4;

	// Rectangle selection vars
	bool _isValid; 
	bool _isDown;
	Vector2 _mouseDownPos;
	Vector2 _mouseLastPos;

	Vector3 _worldDown;
	Vector3 _worldLast;

    // Variables for our selection hotkey/history stuff
    public List<SelectionShell> selectionHistory = new List<SelectionShell>();
	
	// Use this for initialization
	void Start () {
		playerController = GetComponent<PlayerController> ();

        // Maximum of 3 stored selections
        for (int i= 0; i < MaxSelectionHistory; i++)
        {
            selectionHistory.Add(new SelectionShell());
        }
	}

    void clearSelection()
    {
        if (currentSelection.Count > 0)
        {
            for (int i = 0; i < currentSelection.Count; i++)
            {
                try
                {
                    if (currentSelection[i])
                    {
                        ClearSelectionEffect(currentSelection[i]);
                    }
                }
                catch (System.Exception z) { }
            }

            currentSelection.Clear();
        }

        // Also clear GUI related stuff to our selection
        SelectionInterface.instance.clearSelection();
    }

    void VerifySelections()
    {
        // Verify everything in our cache
        for (int i = 0; i < selectionHistory.Count; i++)
        {
            selectionHistory[i].VerifySelection();
        }
    }

    void SaveSelection(int index)
    {
        if (index > MaxSelectionHistory) { return; }

        // Save the current selection
        selectionHistory[index].Set(currentSelection);
    }

    // Returns true or false depending if we have a selection saved in that index
    public bool IsIndexSelected(int index)
    {
        if (index > MaxSelectionHistory) { return false; }

        return selectionHistory[index].HasSelection;
    }

    void RestoreSelection(int index)
    {
        if (index > MaxSelectionHistory) { return; }

        // Clear out our current selection
        clearSelection();

        // Pull out the history selection and apply it
        List<Entity> history = selectionHistory[index].Get();

        foreach (Entity e in history)
        {
            // Select it
            SelectEntity(e);
        }
    }

	void selectMultiple()
	{
		// Find all the entitie's within this location range and select them
        clearSelection();

		Vector3 start = _worldDown;
		Vector3 end = _worldLast;

		if (start.x > end.x) {
			float oldEnd = end.x;
			float oldStart = start.x;

			start.x = oldEnd;
			end.x = oldStart;
		}

		if (start.z > end.z) {
			float oldEnd = end.z;
			float oldStart = start.z;
			
			start.z = oldEnd;
			end.z = oldStart;
		}

		float w = end.x - start.x;
		float h = end.z - start.z;

		Rect selectionRectangle = new Rect (start.x, start.z, w, h);

		List<Entity> allyE = EntityManager.getAllyList ();

		for (int i = 0; i < allyE.Count; i++) {
			if (allyE[i])
			{
				Vector2 point = new Vector2(allyE[i].transform.position.x, allyE[i].transform.position.z);

				if (selectionRectangle.Contains(point))
				{
					//currentSelection.Add (allyE[i]);
					
					//SetSelectionEffect(allyE[i]);

					SelectEntity(allyE[i]);
				}
			}
		}
	}

    public void SelectEntity(Entity target)
    {
        currentSelection.Add(target);
        SetSelectionEffect(target);
    }

    public void SetSelectionEffect(Entity target)
    {
        if (target)
        {
           //target.setOverlayColor(Color.Lerp(target.initialColor, Color.green, 0.5f));
            //target.SetSelectionEffect(true);
            target.isSelected = true;
        }
    }

    public void ClearSelectionEffect(Entity target)
    {
        if (target)
        {
            //target.setOverlayColor(target.initialColor);
            //target.SetSelectionEffect(false);
            target.isSelected = false;
        }
    }

	void selectSingle()
	{
        clearSelection();

		// Left click, try to select a unit
		// Raycast down and see if we hit an entity
		Ray ray = Camera.main.ScreenPointToRay(InputLocation);
		RaycastHit hit;
		bool hitSomething = false; // Use this flag to latter see if we actually hit something
		
		if (Physics.Raycast(ray, out hit))
		{
			Entity hitEntity = hit.collider.GetComponent<Entity>();
			
			if (hitEntity)
			{
				if (hitEntity.locallyOwned){
					// It was an entity and locally owned, so select it
					

					currentSelection.Add(hitEntity);
					
					for (int i = 0; i < currentSelection.Count; i++)
					{
                        SetSelectionEffect(currentSelection[i]);
					}
					
				}
			}
		}
	}

    void MoveSelection(Vector3 target)
    {
        // Group setup.
        Vector3 offset = new Vector3(0, 0, 0);
        float unitSkin = 1.5f; // Space between location picks

        if (currentSelection.Count > 0)
        {
            if (currentSelection.Count == 1) { currentSelection[0].MoveTo(target, true); return; } // If only one is selected back outnow


            // There's units selected so loop through and form a group

            // So if we have 4 then it would be a 2 by 2 grid

            // So the row width is half the total

            // Sqrt the total count so we form boxes of rows/units
            int u_rows = (int)Mathf.Sqrt(currentSelection.Count); u_rows *= 2;//currentSelection.Count / 2;
            int u_rows_i = -1;

            for (int i = 0; i < currentSelection.Count; i++)
            {
				if (currentSelection[i]){
	                u_rows_i++;
	    
	                if (u_rows_i >= u_rows)
	                {
	                    u_rows_i = 0;
	                    // Increment Y offset and reset X
	                    offset.x = 0;
	                    offset.z -= unitSkin;
	                }

	                // This should be considered a "FromUser" action
	                currentSelection[i].MoveTo(target + offset, true);

	                // Increment X offset
	                offset.x += unitSkin;
				}
            }
        }
    }

    public bool IsIntOdd(int value)
    {
        return value % 2 != 0;
    }

    Vector3 getMapPoint(RaycastHit input)
    {
        Vector3 p = input.point;
        Vector3 local = input.collider.transform.InverseTransformPoint(p); // Get the local coordinates of the map click

        Vector3 world = Vector3.zero;

        world.x = local.x / 2;
        world.z = local.y / 2;
        world.y = 1.0f;

        return world;
    }

    public Vector2 InputLocation
    {
        get
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                // Android is unique
                return Input.GetTouch(0).position;
            }
            else
            {
                // Just else the rest to make sure we don't miss a specific version of another platform
                return Input.mousePosition;
            }
        }
    }

    // Some standard methods for breaking into working on android
    public bool IsDownLeft()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            // Android is unique
           // return (Input.GetTouch(0).tapCount == 1);

            return (Input.GetTouch(0).phase == TouchPhase.Began) && (Input.touchCount == 1);
        }
        else
        {
            // Just else the rest to make sure we don't miss a specific version of another platform
            return Input.GetMouseButtonDown(0);
        }
    }

    public bool IsDownRight() // Return true for the equivelant of right click
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            // Android is unique
            return (Input.GetTouch(0).tapCount == 2 || Input.GetTouch(1).tapCount == 2);
        }
        else
        {
            // Just else the rest to make sure we don't miss a specific version of another platform
            return Input.GetMouseButtonDown(1);
        }
    }

    public bool IsUpLeft()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            // Android is unique
            return (Input.GetTouch(0).phase == TouchPhase.Ended);
        }
        else
        {
            // Just else the rest to make sure we don't miss a specific version of another platform
            return Input.GetMouseButtonUp(0);
        }
    }

    public bool IsUpRight() // Return true for the equivelant of right click
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            // Android is unique
            return (Input.GetTouch(0).tapCount == 2);
        }
        else
        {
            // Just else the rest to make sure we don't miss a specific version of another platform
            return Input.GetMouseButtonUp(1);
        }
    }


    System.DateTime lastVerificationTime = System.DateTime.Now; 
    int _mouseDuration = 0;
	// Update is called once per frame
	void Update () {

        UpdateHistoryHotkeys();

        // Every 5 seconds verify our selections
        
        if ((System.DateTime.Now - lastVerificationTime).Seconds > 5)
        {
            lastVerificationTime = System.DateTime.Now;

            // Verify the selections
            VerifySelections();
        }

        // Two main tree's here.  We're either handling the world or the mouse is over the GUI
        if (SelectionInterface.instance.mouseOverGUI(InputLocation))
        {
            // If our mouse was down emulat as if we just realsed it
            if (_isDown)
            {
                _mouseLastPos = InputLocation;
                _worldLast = playerController.mouseWorld;
                _isDown = false;
                _isValid = false;
                selectMultiple();
            }

            // Not much to really do, just blocking update/clear of selection mostly.

            // See if the mouse is over the mini map
            if (SelectionInterface.instance.lastMouseHit.collider.tag == "MAP")
            {
                // Left click <- send camera to this point
                if (IsDownLeft())
                {
                    Vector3 mp = getMapPoint(SelectionInterface.instance.lastMouseHit);

                    playerController.SetLocation(mp);
                }

                // Right click <- send units to this point
                if (IsDownRight())
                {
                    Vector3 mp = getMapPoint(SelectionInterface.instance.lastMouseHit);

                    MoveSelection(mp);

                }
            }

        }
        else
        {
            // No dragging on android really
            if (Application.platform == RuntimePlatform.Android)
            {
                /*
                if (IsDownLeft())
                {
                    _mouseDownPos = InputLocation;
                    _worldDown = playerController.mouseWorld;
                }

                bool retain = false;
                if (IsDownRight())
                {
                    retain = true;
                    MoveSelection(playerController.mouseWorld + new Vector3(0, 0.4f, 0));
                }

                if (retain == false)
                {
                    if (IsUpLeft())
                    {
                        selectSingle(); // Try selecting it
                    }
                }
                */

                // Double tap moves units
                if (Input.touchCount == 2)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved)
                    {
                        // Move the camera
                        playerController.targetLocation += new Vector3(Input.GetTouch(0).deltaPosition.x, 0, Input.GetTouch(0).deltaPosition.y) * playerController.MoveSpeed * Time.smoothDeltaTime;
                    }
                    else
                    {
                        MoveSelection(playerController.mouseWorld + new Vector3(0, 0.4f, 0));
                    }
                }

                // Check for release on tap
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    if (_mouseDuration > 0 && _mouseDuration < mouseDragThreshold)
                    {
                        // It was a single click!
                        selectSingle();

                        _mouseDuration = 0;
                    }
                }

                if (Input.GetTouch(0).phase == TouchPhase.Began && Input.touchCount == 1)
                {
                    // Just touched
                    _mouseDownPos = InputLocation;
                    _worldDown = playerController.mouseWorld;
                    _isDown = true;
                    _isValid = true;
                }

                if (_isDown)
                {
                    _mouseDuration++;

                    _mouseLastPos = InputLocation;
                    _worldLast = playerController.mouseWorld;


                    if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    {
                        _isDown = false;
                        _isValid = false;

                        if (_mouseDuration >= mouseDragThreshold)
                        {
                            selectMultiple();

                            _mouseDuration = 0;
                            // If you want the box to vanish now, just set 'isValid' to false.
                        }
                    }
                }
            }
            else
            {
                // On right tell our selection to move
                if (IsDownRight())
                {
                    MoveSelection(playerController.mouseWorld + new Vector3(0, 0.4f, 0));
                }

                // On release try for a single click condition
                if (IsUpLeft())
                {
                    if (_mouseDuration > 0 && _mouseDuration < mouseDragThreshold)
                    {
                        // It was a single click!
                        selectSingle();

                        _mouseDuration = 0;
                    }
                }


                // Selection related stuff
                if (IsDownLeft())
                {
                    _mouseDownPos = InputLocation;
                    _worldDown = playerController.mouseWorld;
                    _isDown = true;
                    _isValid = true;
                }

                // Continue tracking mouse position until the button is raised
                if (_isDown)
                {
                    _mouseDuration++;

                    //Debug.Log(_mouseDuration);
                    _mouseLastPos = InputLocation;
                    _worldLast = playerController.mouseWorld;


                    if (IsUpLeft())
                    {
                        _isDown = false;
                        _isValid = false;

                        if (_mouseDuration >= mouseDragThreshold)
                        {
                            selectMultiple();

                            _mouseDuration = 0;
                            // If you want the box to vanish now, just set 'isValid' to false.
                        }
                    }
                }
            }
        }
	}

    void UpdateHistoryHotkeys()
    {
        // Ctrl + 0-9, saves selection. 0-9 restores the selection

        bool hasRegistered = false;

        // Check hotkeys for selection saving
        if (Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha1))
        {
            hasRegistered = true;
            SaveSelection(0);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha2))
        {
            hasRegistered = true;
            SaveSelection(1);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha3))
        {
            hasRegistered = true;
            SaveSelection(2);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha4))
        {
            hasRegistered = true;
            SaveSelection(3);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha5))
        {
            hasRegistered = true;
            SaveSelection(4);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha6))
        {
            hasRegistered = true;
            SaveSelection(5);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha7))
        {
            hasRegistered = true;
            SaveSelection(6);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha8))
        {
            hasRegistered = true;
            SaveSelection(7);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha9))
        {
            hasRegistered = true;
            SaveSelection(8);
        }

        if (hasRegistered == false)
        {
            // Restore selection hotkeys
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                RestoreSelection(0);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                RestoreSelection(1);
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                RestoreSelection(2);
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                RestoreSelection(3);
            }
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                RestoreSelection(4);
            }
            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                RestoreSelection(5);
            }
            if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                RestoreSelection(6);
            }
            if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                RestoreSelection(7);
            }
            if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                RestoreSelection(8);
            }
        }
    }

	void OnGUI()
	{
		// Render selection rectangle
		if (_isValid) {
			// Find the corner of the box
			Vector2 origin;
			origin.x = Mathf.Min(_mouseDownPos.x, _mouseLastPos.x);
			// GUI and mouse coordinates are the opposite way around.
			origin.y = Mathf.Max(_mouseDownPos.y, _mouseLastPos.y);
			origin.y = Screen.height - origin.y; 
			//Compute size of box
			Vector2 size = _mouseDownPos - _mouseLastPos;
			size.x = Mathf.Abs(size.x);
			size.y = Mathf.Abs(size.y);   
			// Draw it!
			Rect r = new Rect(origin.x, origin.y, size.x, size.y);
			GUI.Box(r, "");
		}
	}
}

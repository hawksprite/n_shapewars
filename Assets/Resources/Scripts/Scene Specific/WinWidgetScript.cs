﻿using UnityEngine;
using System.Collections;

public class WinWidgetScript : MonoBehaviour {

    UIWidget widget;

	void Awake(){
        widget = GetComponent<UIWidget>();
        widget.alpha = 0.0f;
	}

	void Update () {
        bool allDead = true;

        for (int i = 0; i < PlayerStats.instance.pssCache.Count; i++)
        {
            if (PlayerStats.instance.pssCache[i].Name != PlayerStats.instance._player.Name)
            {
                if (PlayerStats.instance.pssCache[i].Alive)
                {
                    allDead = false;
                }
            }
        }

        if (allDead)
        {
            widget.alpha = 1.0f;
        }
	}
}

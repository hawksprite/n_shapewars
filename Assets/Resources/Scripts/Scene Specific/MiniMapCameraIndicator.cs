﻿using UnityEngine;
using System.Collections;

public class MiniMapCameraIndicator : MonoBehaviour {

    public GameObject cameraRoot;
    public UIAnchor anchor;

    public Vector2 anchorOffset = new Vector2(10, 10);

	void Awake(){

	}

	void Start () {
	
	}
	
	void Update () {
        float newX = cameraRoot.transform.position.x * 2;//(cameraRoot.transform.position.x / 100) * 175.0f;
        float newY = cameraRoot.transform.position.z * 2;//(cameraRoot.transform.position.z / 100) * 175.0f;

        anchor.pixelOffset = new Vector2(newX + anchorOffset.x, newY + anchorOffset.y);
	}
}

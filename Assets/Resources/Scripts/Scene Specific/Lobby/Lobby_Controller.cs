﻿using UnityEngine;
using System.Collections;

public class Lobby_Controller : MonoBehaviour {

    public float rotationSpeed = 10.0f;

    public bool QuickStart = true;
    private bool _t = false;
    private int _tc = 0;

    // Important links to Label's and such for the lobby
    public UILabel playerSlot1;
    public UILabel playerSlot2;
    public UILabel playerSlot3;
    public UILabel playerSlot4;

    public UISprite playerSlotC1;
    public UISprite playerSlotC2;
    public UISprite playerSlotC3;
    public UISprite playerSlotC4;

    public UIButton launchButton;

    public UILabel chatLabel;

    public UIInput chatInput;

    private string c_S1 = "Empty";
    private string c_S2 = "Empty";
    private string c_S3 = "Empty";
    private string c_S4 = "Empty";

	// Use this for initialization
	void Start () {
        // Make sure we're actually connected
        if (PhotonNetwork.connected == false)
        {
            Application.LoadLevel(0);
            return;
        }

	    // See if we're the host, if not disable the launch button
        if (!PhotonNetwork.isMasterClient)
        {
            launchButton.isEnabled = false;
        }
        else
        {
            // Safely can enable it
            launchButton.isEnabled = true;
        }
	}

    void HadAction(string name)
    {
        if (name == "chat-submit")
        {
            // Ignore empty submissions
            if (chatInput.value != "")
            {
                // Chat was entered
                ChatController.instance.SendChat(chatInput.value);

                // Clear text
                chatInput.value = "";
            }
        }

        if (name == "launch")
        {
			Debug.Log("Trying to launch --- " + PhotonNetwork.connectionState.ToString ());

            // Game launch attempted
            if (PhotonNetwork.isMasterClient)
            {
				Debug.Log ("Loading level...");

                // Let photon know this game isn't joinable anymore
                PhotonNetwork.room.open = false;
                PhotonNetwork.room.visible = false;

                // Launch the game!
                PhotonNetwork.LoadLevel(1); // Test bed
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	    // Slowly rotate the lobby camera for the skybox
        transform.Rotate(new Vector3(0, rotationSpeed * Time.smoothDeltaTime, 0));

        // Check our connected players to see if we should update the slot text
        if (PhotonNetwork.playerList.Length != 0)
        {
            // We have someone in the room, try for used slot
            c_S1 = testList(PhotonNetwork.playerList, 0);
            c_S2 = testList(PhotonNetwork.playerList, 1);
            c_S3 = testList(PhotonNetwork.playerList, 2);
            c_S4 = testList(PhotonNetwork.playerList, 3);
        }

        // Update our chat label to the current cache
        chatLabel.text = ChatController.instance.ChatCache;

        // If our slot text cache is diffrent update the label text
        compareLabel(playerSlot1, c_S1);
        compareLabel(playerSlot2, c_S2);
        compareLabel(playerSlot3, c_S3);
        compareLabel(playerSlot4, c_S4);

        if (QuickStart)
        {
            if (_t == false)
            {
                _tc++;

                if (_tc >= 60)
                {
                    _t = true;
                    HadAction("launch");
                }
            }
        }
	}

    void compareLabel(UILabel label, string content)
    {
        if (label.text == content)
        {
            return; // No need
        }
        else
        {
            // Update it
            label.text = content;
        }
    }

    string testList(PhotonPlayer[] currentList, int id)
    {
        if (currentList.Length > id)
        {
            string name = currentList[id].name;
            PSS pss = PlayerStats.instance.GetPSS(name);

            // Assign the color for this ID
            switch(id)
            {
                case 0: playerSlotC1.color = pss.Color; break;
                case 1: playerSlotC2.color = pss.Color; break;
                case 2: playerSlotC3.color = pss.Color; break;
                case 3: playerSlotC4.color = pss.Color; break;
            }

			return name;
        }
        else
        {
            return "Empty";
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class Lobby_ColorToggle : MonoBehaviour {

	void OnClick()
    {
        // On click call to randomize this player's color
        PlayerStats.instance.RandomizePlayerColor();
    }
}

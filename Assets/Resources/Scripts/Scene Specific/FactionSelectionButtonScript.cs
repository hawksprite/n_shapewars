﻿using UnityEngine;
using System.Collections;

public class FactionSelectionButtonScript : MonoBehaviour {

    UIButton button;
    public UISprite back;

	void Awake(){
        button = GetComponent<UIButton>();
	}

	void Start () {
        setTexture();
	}

    void OnClick()
    {
        PlayerStats.instance._player.Faction++;

        if (PlayerStats.instance._player.Faction >= 3) { PlayerStats.instance._player.Faction = 0; }

        setTexture();
    }

    void setTexture()
    {
        string set = "Cuboid";
        switch(PlayerStats.instance._player.Faction)
        {
            case 0: set = "Cuboid"; break;
            case 1: set = "Circle"; break;
            case 2: set = "Triangle"; break;
        }

        button.normalSprite = set;
        button.hoverSprite = set;
        button.disabledSprite = set;
        button.pressedSprite = set;
        back.spriteName = set;
    }
}

﻿using UnityEngine;
using System.Collections;

public class LoseWidgetScript : MonoBehaviour {

    UIWidget widget;

	void Awake(){
        widget = GetComponent<UIWidget>();
        widget.alpha = 0.0f;
    }

	
	void Update () {
	    if (PlayerStats.instance._player.Alive == false)
        {
            widget.alpha = 1.0f;
        }
	}
}

﻿using UnityEngine;
using System.Collections;
using System;

public class BasicTankTurretAI : MonoBehaviour {

	Quaternion originalRotation;
    private Entity parentEntity;

    public Vector3 RotationOffset = new Vector3(0, 0, 0);

	// Use this for initialization
	void Start () {
		originalRotation = transform.rotation;

        // Find the parent entity
        // TODO, hard coded for the weapon always being a child to the entity
        parentEntity = transform.parent.gameObject.GetComponent<Entity>();
	}
	

	// This is what the entity would call down to each frame that we have an active target we're agroing on
    DateTime lastFireTime = DateTime.Now;

	int attackTimer = 0;
	void UpdateWeapon(Entity target)
	{
        // Only update our weapon if we're the owner
        if (parentEntity.locallyOwned)
        {
            // Look at target, zero out X and Y, apply offset, re-apply.
            transform.LookAt (target.transform.position);
            Quaternion q = Quaternion.Euler(new Vector3(0 + RotationOffset.x,transform.rotation.y + RotationOffset.y,0 + RotationOffset.z));
            transform.rotation = q;

            // The attack speed is milliseconds per fireing of the weapon, so check to see if that time has come
            if ((DateTime.Now - lastFireTime).TotalMilliseconds >= parentEntity.settings.AttackSpeed)
            {
                lastFireTime = DateTime.Now;

                // Call for a fire on that target
                parentEntity.FireWeapon(target);
            }
        }
	}

}

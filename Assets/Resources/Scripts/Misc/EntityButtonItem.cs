﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class EntityButtonItem  {
    public string ButtonTooltip = "Stop movement";
    public string ButtonImage = "stopButtonImage";
    public string ButtonAction = "hold";
}

﻿using UnityEngine;
using System.Collections;

public class TrigonFactory : MonoBehaviour {

	Entity entity;

	// Use this for initialization
	void Start () {
		entity = GetComponent<Entity> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnAction(string action)
	{
		if (action == "build-hail") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Hail, transform.position, Quaternion.identity, entity);
		}

		if (action == "build-infantry") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Infantry, transform.position, Quaternion.identity, entity);
				}

		if (action == "build-mortar") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Mortar, transform.position, Quaternion.identity, entity);
				}
		if (action == "build-trebuche") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Trebuche, transform.position, Quaternion.identity, entity);
		}
		if (action == "build-glaive") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Glaive, transform.position, Quaternion.identity, entity);
		}
		if (action == "build-devourer") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Devourer, transform.position, Quaternion.identity, entity);
		}

		if (action == "build-dragonfly") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Dragonfly, transform.position, Quaternion.identity, entity);
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class TrigonCommand : MonoBehaviour {

	Entity e;

	// Use this for initialization
	void Start () {
		e = GetComponent<Entity> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnAction(string name)
	{
		if (name == "build-worker") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Worker, transform.position, Quaternion.identity, e);
		}
	}
}

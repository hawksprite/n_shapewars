﻿using UnityEngine;
using System.Collections;

public class SphereFactory : MonoBehaviour {

	Entity e;

	// Use this for initialization
	void Start () {
		e = GetComponent<Entity> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnAction(string action)
	{
		if (action == "build-ama") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Ama, transform.position, Quaternion.identity, e);
		}
		if (action == "build-healer") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Healer, transform.position, Quaternion.identity, e);
		}
		if (action == "build-infantry") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Infantry, transform.position, Quaternion.identity, e);
		}
		if (action == "build-reaper") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Reaper, transform.position, Quaternion.identity, e);
		}
		if (action == "build-saucer") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Saucer, transform.position, Quaternion.identity, e);
		}
		if (action == "build-titan") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Titan, transform.position, Quaternion.identity, e);
		}
		if (action == "build-dragoon") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Dragoon, transform.position, Quaternion.identity, e);
		}
		if (action == "build-rot") {
			GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Rotatron, transform.position, Quaternion.identity, e);
		}
	}
}

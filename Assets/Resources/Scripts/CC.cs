﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CC : MonoBehaviour {

    [HideInInspector]
    public Entity entity;

    void Awake()
    {
        entity = GetComponent<Entity>();
    }

	void Start () {
        //gameObject.SendMessage("RegisterSelectionAction", "build-worker|workerI");
	}
	
	// Update is called once per frame
	void Update () {

        //animationLink.transform.Rotate(new Vector3(0, 0, 0.7f));
	    
	}

    public void AcceptResources(int ammount)
    {
        // Add em in!
        Stats.RecieveVectorites(ammount);
    }

    void OnAction(string action)
    {
        if (action == "build-worker")
        {
			switch (PlayerStats.Player.Faction)
			{
			case 0: GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CUBE_Worker, transform.position, Quaternion.identity, this.entity);break;
			case 1: GeneralControll.instance.SpawnUnit(UnitCatalog.Units.CIR_Worker, transform.position, Quaternion.identity, this.entity);break;
			case 2: GeneralControll.instance.SpawnUnit(UnitCatalog.Units.TRI_Worker, transform.position, Quaternion.identity, this.entity);break;
			}
            
        }
    }
}

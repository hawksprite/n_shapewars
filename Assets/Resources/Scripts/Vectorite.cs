﻿using UnityEngine;
using System.Collections;

public class Vectorite : MonoBehaviour {

	private float Yield = 12.0f;
    public float VectoriteBank = 50000.0f;

    public bool Empty
    {
        get
        {
            if (VectoriteBank > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    public float GetYield(float drillingPower)
    {
        // Reduce it from our back
        VectoriteBank -= drillingPower;

        if (VectoriteBank > 0)
        {
            return drillingPower;
        }
        else
        {
            return 0.0f;
        }
    }

    bool switchedColor = false;
	// Update is called once per frame
	void Update () {
	    if (Empty)
        {
            // If empty switch to a gray color
            if (switchedColor == false)
            {
                switchedColor = true;

                Renderer[] cR = gameObject.GetComponentsInChildren<Renderer>();
                foreach (Renderer r in cR)
                {
                    r.material.color = Color.gray;
                }
            }
        }
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DroneAttack : MonoBehaviour {

	private float xtar = 0.0f;
	private float ztar = 0.0f;
	private int timah = 0;
	private bool secondStage = false;

	// Use this for initialization
	void Start () {
		transform.parent.Rotate (new Vector3(90, 0, 0));
	}
	
	// Update is called once per frame
	void Update () {
		if (timah > 1) {
			timah -= 1;
			this.transform.Translate(0, 0, -0.5f);
		}
		if (timah > 2 && timah < 5) {
			transform.position = new Vector3(xtar + Random.Range(-2, 2), 100, ztar + Random.Range(-2, 2));

			secondStage = true;
			timah = -100;
		}
		if (secondStage == true) {
			this.transform.Translate (0, 0, 0.5f);
		}
		if (this.transform.position.y < 1.0f) {
			if(secondStage){
				List<Entity> damaged = EntityManager.getEnemyList ();
				foreach (Entity e in damaged) {
					float distance = Vector3.Distance(e.transform.position, transform.position);
					
					if (distance <= 20){
						float f = 40 + (60 - (distance*2.5f));
						int i = (int)f;
						e.TakeDamage(i);
					}
				}

				ParticleManager.instance.PlayParticle(0, transform.position);

				Destroy (gameObject);

			}
		}
	}

	void Attack (Vector3 pointage) {
		xtar = pointage.x;
		ztar = pointage.z;
		timah = 120 + Random.Range(0, 60);

	}
}

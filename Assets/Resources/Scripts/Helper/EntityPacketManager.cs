﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EntityPacketManager : MonoBehaviour {

    private static EntityPacketManager _instance;

    //This is the public reference that other classes will use
    public static EntityPacketManager instance
    {
        get
        {
            //If _instance hasn't been set yet, we grab it from the scene!
            //This will only happen the first time this reference is used.
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<EntityPacketManager>();
            return _instance;
        }
    }

    public string lastSentBuffer = "";

    public void SendBuffer(string data)
    {
        handlerStoreBuffer(data);
    }

    void Start() { sync = GetComponent<EPMSync>(); }


    [HideInInspector]
    public EPMSync sync;

    int currentRecieving = 0;
    public List<PacketEncap> futureBuffers = new List<PacketEncap>();

    public List<PacketAlert> futureAlerts = new List<PacketAlert>();

    void handlerStoreBuffer(string incoming)
    {
        if (futureBuffers.Count == 0) { futureBuffers.Add(new PacketEncap()); currentRecieving = 0; }

        int bufferIndex = futureBuffers.Count - 1;

        futureBuffers[bufferIndex].packetCount++;
        futureBuffers[bufferIndex].buffer += incoming + "&";

        if (futureBuffers[bufferIndex].packetCount > GlobalConfiguration.NetworkMaxPacketsPerPackage)
        {
            futureBuffers.Add(new PacketEncap()); // Bump up so we start a new encap
        }

        // If our buffer got above the set max, then dump the lowest
        if (futureBuffers.Count > GlobalConfiguration.NetworkMaxPackageQue)
        {
            futureBuffers.RemoveAt(0);
        }
    }

    string handleGetLatestPacket()
    {
        // See if the buffer has anything, if so remove at location 0.  Delete from array and send back.
        string t = "";

        if (futureBuffers.Count == 0) { return ""; }

        t = futureBuffers[0].buffer;

        futureBuffers.RemoveAt(0);

        return t;
    }


    DateTime lastBufferCheck = DateTime.Now;
    void LateUpdate()
    {
        // Use late update so the buffer send check always happens at the tail end of the frame.
        if ((DateTime.Now - lastBufferCheck).TotalMilliseconds >= GlobalConfiguration.NetworkPackageMillisecondDelay)
        {
            lastBufferCheck = DateTime.Now;
            string frameBuffer = handleGetLatestPacket();

            if (frameBuffer == "") { return; } // Make sure there's something

            sync.fromPushData(frameBuffer);

            // Clear the frame buffer
            lastSentBuffer = frameBuffer; // Store this for GUI stuff.
            frameBuffer = "";
        }

        List<PacketAlert> lastingAlerts = new List<PacketAlert>();
        // If the alert que has anything check it
        for (int i = 0; i < futureAlerts.Count; i++)
        {
            if (!AlertTarget(futureAlerts[i].target, futureAlerts[i].data, false))
            {
                // If it wasn't succesfull then add keep it in the que
                lastingAlerts.Add(futureAlerts[i]);
            }
        }
    }



    public bool AlertTarget(string target, string data, bool queOnFail)
    {
        GameObject t = findGameObject(target);

        if (t)
        {
            t.SendMessage("RecieveInfo", data);

            // Is good
            return true;
        }

        if (queOnFail)
        {
            // Que this for later since it didn't go through
            futureAlerts.Add(new PacketAlert(target, data));
        }

        // No go on the cherios
        return false;
    }


    //List<GameObject> objectLibrary = new List<GameObject>();

    GameObject findGameObject(string name)
    {
        return GameObject.Find(name); // Might need to optimize
    }
}

public class PacketEncap
{
    public string buffer;
    public int packetCount = 0;
}

public class PacketAlert
{
    public string target;
    public string data;

    public PacketAlert(string t, string d)
    {
        target = t;
        data = d;
    }
}
﻿using UnityEngine;
using System.Collections;
using System;

public static class NetworkTools {
    // This class house's some utility functions for converting data types to string and back, useful for RPC calls to minimalize bandwith

    // House some "Last" of values here as a kind of cache for possible utility in network prediction
    public static Vector3 CACHE_LastConversion = new Vector3(0, 0, 0);

    public static string ColorToString(Color Input)
    {
        string buffer = "";

        buffer += Input.r + ",";
        buffer += Input.g + ",";
        buffer += Input.b;

        return buffer;
    }

    public static Color ColorFromString(string input)
    {
        float r = 1.0f;
        float g = 1.0f;
        float b = 1.0f;

        string[] bits = input.Split(',');

        // We're going to be parsing into float from data we got network side, expect the worst.
        try
        {
            r = float.Parse(bits[0]);
            g = float.Parse(bits[1]);
            b = float.Parse(bits[2]);
        }
        catch (System.Exception z) { }

        Color buffer = new Color(r, g, b, 1);
        
        return buffer;
    }

    public static string Vector3ToString(Vector3 Input)
    {
        string buffer = "";

        buffer += Input.x + ",";
        buffer += Input.y + ",";
        buffer += Input.z + "";

        return buffer;
    }

    
    public static Vector3 Vector3FromString(string input)
    {
        Vector3 buffer = Vector3.zero;

        try
        {
            string[] bits = input.Split(',');

            buffer.x = float.Parse(bits[0]);
            buffer.y = float.Parse(bits[1]);
            buffer.z = float.Parse(bits[2]);

            // Store in cache then send it back
            CACHE_LastConversion = buffer;

            return buffer;
        }
        catch(Exception z)
        {
            // If things hit the fan there's network issues.  Don't just crash and burn though.
            // Return whatever's in our cache for last convereted location
            return CACHE_LastConversion;
        }

        return buffer;
    }
}

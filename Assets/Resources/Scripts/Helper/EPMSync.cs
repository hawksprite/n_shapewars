﻿using UnityEngine;
using System.Collections;

public class EPMSync : Photon.MonoBehaviour {

	// Use this for initialization
    void Awake()
    {
        
    }

    void Start()
    {
        // If it's us
        if (photonView.owner.name == PlayerStats.Player.Name)
        {
            gameObject.AddComponent<EntityPacketManager>();
        }
    }

    public void fromPushData(string buffer)
    {
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            if (!PhotonNetwork.playerList[i].isLocal)
            {
                photonView.RPC("RecieveData", PhotonNetwork.playerList[i], buffer);
            }
        }
    }

    [RPC]
    void RecieveData(string packet, PhotonMessageInfo info)
    {
        // Break them down into each packet
        string[] bits = packet.Split('&');

        // Send out the recieving packet to each target
        foreach (string p in bits)
        {
            string[] segments = p.Split('|');

            string target = segments[0];

            string l = info.sender.name + "|" + p; // Add the sender's name to the front of it, no bandwith hits since it's strictly local from here on out.

            EntityPacketManager.instance.AlertTarget(target, l,true); // Flag to que if it fails
        }
    }
}

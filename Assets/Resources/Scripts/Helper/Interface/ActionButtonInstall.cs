﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ActionButtonInstall : MonoBehaviour {

    public int rows = 3;
    public int columns = 5;
    public GameObject actionButtonPrefab;

    public Vector2 originOffset;
    public float shiftDepth = 80;

	// Use this for initialization
	void Start () {

	}

    [ContextMenu("Setup Buttons")]
    void InstallButtons()
    {
        Vector2 spot = originOffset;

        int spawnIndex = 0;
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < columns; x++)
            {
                // Spawn a buttton, and set it to a child of this scripts attached object
                GameObject g = GameObject.Instantiate(actionButtonPrefab) as GameObject;
                g.transform.parent = this.transform;

                // Run the button setup
                ActionButtonSetup a = g.GetComponent<ActionButtonSetup>();

                a.SetupButton(spot, this.gameObject, spawnIndex); // The spawn index allows us to more easily keep track of clicks in the selection interface

                spot.x += shiftDepth;

                spawnIndex++;
            }

            spot.x = originOffset.x; // Reset to the 50 origin
            spot.y -= shiftDepth; // Bump down to the next row
        }
    }
}

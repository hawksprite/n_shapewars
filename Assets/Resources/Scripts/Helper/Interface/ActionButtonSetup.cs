﻿using UnityEngine;
using System.Collections;

public class ActionButtonSetup : MonoBehaviour {

    public UIAnchor buttonAnchor;
    public BoxCollider boxCollider;
    public Vector2 boxColliderSize;
    public Vector3 originReset;

    public UIWidget tooltipWidget;
    public UILabel tooltipLabel;

    public int ActionButtonIndex = 0;

    private bool hasRegistered = false;

    // We should also register some useful links for later interfacing with this
    public UISprite actionSprite;

	[HideInInspector]
	public UISprite buttonBackgroundSprite;
	[HideInInspector]
	public UIButton rootButton;

	public void SetupButton(Vector2 offset, GameObject installer, int index)
    {
        // Setup the button, it's grid identifier is the given index

        buttonAnchor = GetComponent<UIAnchor>();
        boxCollider = GetComponent<BoxCollider>();

        // Start by getting this object into it's "Origin" settings.  Where we have the proper location, scaleing and such.

        buttonAnchor.container = installer;

        this.transform.position = originReset;
        this.transform.localScale = new Vector3(1, 1, 1);

        buttonAnchor.pixelOffset = offset;
        
        // Clean up the box collider
        boxCollider.center = Vector3.zero;
        boxCollider.size = boxColliderSize;

        // Should be set.
        ActionButtonIndex = index;

		actionSprite.alpha = 0.0f;
    }

    void Start()
    {
        // When the object actually start's up in a scene this is where we will register with the selection interface
        SelectionInterface.instance.RegisterButton(this);

        // Link up the UISprite in our second child
        actionSprite = transform.GetChild(1).GetComponent<UISprite>();
        actionSprite.SetDimensions(60, 60);

		rootButton = GetComponent<UIButton> ();
		buttonBackgroundSprite = transform.GetChild (0).GetComponent<UISprite> ();

        ClearSelectionImage();
    }

    void OnHover( bool isOver )
    {
        if (isOver)
        {
            if (hasRegistered)
            {
                tooltipWidget.alpha = 1.0f;
            }
        }
        else
        {
            tooltipWidget.alpha = 0.0f;
        }

    }

    void OnClick()
    {
        // Report this to the selection interface
        SelectionInterface.instance.AbilityButtonPressed(this);
    }

    public void ClearSelectionImage()
    {
        //NGUITools.SetActive(actionSprite.gameObject, false);
        hasRegistered = false;
        tooltipWidget.alpha = 0.0f;

		if (buttonBackgroundSprite) {
			buttonBackgroundSprite.spriteName = "SquareButtonNormal";
		}

		if (rootButton) {
						rootButton.normalSprite = "SquareButtonNormal";
						rootButton.disabledSprite = "SquareButtonNormal";
						rootButton.hoverSprite = "SquareButtonNormal";
						rootButton.pressedSprite = "SquareButtonNormal";
				}

		actionSprite.alpha = 0.0f;
    }

    public void SetSelectionImage(string sprite)
    {
        //NGUITools.SetActive(actionSprite.gameObject, true);
        //actionSprite.spriteName = sprite;
        hasRegistered = true;

		if (buttonBackgroundSprite) {
			buttonBackgroundSprite.spriteName = sprite;
		}

		if (rootButton) {
						rootButton.normalSprite = sprite;
						rootButton.disabledSprite = sprite;
						rootButton.hoverSprite = sprite;
						rootButton.pressedSprite = sprite;
				}
		actionSprite.alpha = 0.0f;
    }

    public void SetTooltipText(string text)
    {
        tooltipLabel.text = text;
    }

    public void ClearTooltipText()
    {
        tooltipLabel.text = "";
    }
}

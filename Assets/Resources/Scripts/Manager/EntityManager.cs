﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class EntityManager {

	private static List<Entity> enemyEntitys = new List<Entity>();
    private static List<Entity> allyEntitys = new List<Entity>();

	public static void Start()
	{
		// Clear our enemy entity list
		enemyEntitys.Clear ();
	}

	public static void Update(Entity[] objectList)
	{
		// Clear and repopulate our list
		enemyEntitys.Clear ();
        allyEntitys.Clear();

		// Add all of the "Enemys" to our enemy list
		foreach (Entity e in objectList) {
			if (e.currentSide == Entity.Side.Enemy)
			{
				enemyEntitys.Add (e);
			}
            else
            {
                allyEntitys.Add(e);
            }
		}
	}

	public static List<Entity> getEnemyList()
	{
		return enemyEntitys;
	}

    public static List<Entity> getAllyList()
    {
        return allyEntitys;
    }

}

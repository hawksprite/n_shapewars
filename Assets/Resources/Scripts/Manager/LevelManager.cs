﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

    // Make the level manager a presistent singletone object
    private static LevelManager _instance;
    public static LevelManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<LevelManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }
 

    // We'll also store some player configuration right here


    // Some utility functions
    public void ExitGame()
    {
        // Make sure anything that needs cleaning is handled

        Application.Quit();
    }

    void Update()
    {
        // Check for some global hotkey's here.  This object never dies since it serves as a scene transporter.
    }

    public void MainMenu()
    {
        // We need to kill the only persistent object then push back to the main menu
        if (PlayerStats.instance.gameObject)
        {
            GameObject.Destroy(PlayerStats.instance.gameObject);
        }

        // Also if we're connected to photon before we load the root scene disconnect
        if (PhotonNetwork.connected)
        {
            PhotonNetwork.Disconnect();
        }

        Application.LoadLevel(0);
    }
}

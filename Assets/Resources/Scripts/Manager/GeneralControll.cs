﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Units = UnitCatalog.Units;

public class GeneralControll : MonoBehaviour {

	// General controll script, we can manage pretty broad things like income and checking for connection loss.
    // Also this is where all scripts can make central calls for specific units

    private static GeneralControll _instance;
    // This puppy makes sure that when we're statically called from other scripts that we make sure this isn't a null version of the General Controller.
    public static GeneralControll instance { get { if (_instance == null) { _instance = GameObject.FindObjectOfType<GeneralControll>(); } return _instance; } }

    // Links to objects for easy access cross game
    public MapConfiguration mapConfiguration; // Link to the current map configuation.  It's set at the head of this objects startup method.
    public GameObject cameraRoot; // Useful for forcing the gamera into a location on spawn
    public PlayerController playerController;
    public GameObject pauseMenuRoot;

    // To keep the links and such above clean we'll put all the script var's down here
    private Vector3 playerSpawnLocation;
    private bool activePause = false;

    // Our enum for units


	// Use this for initialization
	void Start () {
		if (!PhotonNetwork.connected) {
			// No connection?  Kick em back to scene 0
			Application.LoadLevel(0);
		}
        else
        {
            // Hide our pause menu right off
            SetPauseMenu(activePause);

            UnitCatalog.instance.PrepareList();

            // Spawn our packet manager
            PhotonNetwork.Instantiate("PacketManager", Vector3.zero, Quaternion.identity, 0);

            // Find any objects we provide links to with the GC
            mapConfiguration = FindObjectOfType<MapConfiguration>();
            cameraRoot = GameObject.FindGameObjectWithTag("RootCamera");
            playerController = FindObjectOfType<PlayerController>();

            // If any cruicial links aren't found the scene wasn't fully setup correctly.
            if (!mapConfiguration || !cameraRoot || !playerController)
            {
                Debug.Log("ALERT: Check your GeneralControl startup script because a cruicial linked object wasn't found!");
            }

            // For this player we need to spawn some basic stuff to get them going.

            // Setup all our starting stuff
            Stats.SetVectorites(900);

            // Pick and set a spawn location
            playerSpawnLocation = mapConfiguration.GetRandomSpawnLocation();
            playerController.SetLocation(playerSpawnLocation);

            // Let's go ahead and throw down a command center for the user
            Vector3 ccLocation = playerSpawnLocation;
            ccLocation.y = 0;

            // Spawn our command center at the camera spawn location
			if (PlayerStats.instance._player.Faction == 0){
            	SpawnUnit(Units.CUBE_Command, ccLocation, Quaternion.identity, null);
			}
			if (PlayerStats.instance._player.Faction == 1){
				SpawnUnit(Units.CIR_Command, ccLocation, Quaternion.identity, null);
			}
			if (PlayerStats.instance._player.Faction == 2){
				SpawnUnit(Units.TRI_Command, ccLocation, Quaternion.identity, null);
			}
        }
	}

    // We'll house the utility for the pause menu here
    public void SetPauseMenu(bool active)
    {
        pauseMenuRoot.SetActive(active);
    }
	
	// Main mono-behavior centric update loop for handling any time based login/network related stuff
	void Update () {
		
        // Some handy keyboard shortcuts in game

        // Check for cheats
        UpdateCheats();

        if (Input.GetKeyDown(KeyCode.Home) || Input.GetKeyDown(KeyCode.Space)) // Home button shoots you back to where your camera started at the begining of the match.
        {
            playerController.SetLocation(playerSpawnLocation);
        }

        // Hotkeys for some global configuration stuff
        if (Input.GetKeyDown(KeyCode.LeftBracket))
        {
            GlobalConfiguration.CameraMoveSpeed -= 1;
        }

        if (Input.GetKeyDown(KeyCode.RightBracket))
        {
            GlobalConfiguration.CameraMoveSpeed += 1;
        }

        // Toggle our pause menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Invert the active flag
            activePause = !activePause;

            // Apply it
            SetPauseMenu(activePause);
        }

        if (EntityManager.getAllyList().Count == 0)
        {
            PlayerStats.Player.Alive = false;
        }

        // We'll be pushing some info into our PSS here as well.

        PlayerStats.Player.UnitCount = Stats.ownedUnits;
        PlayerStats.Player.CameraLocation = cameraRoot.transform.position;
	}

    void UpdateCheats()
    {
        if (GlobalConfiguration.AllowCheats)
        {
            if (Input.GetKeyDown(KeyCode.PageUp))
            {
                PlayerStats.Player.Vectorites += 5000;
            }
        }
    }

    // Utility/Public functions.  Anything other classes will be banking on using for centric info.
    public UnitCatalogItem GetUnitPrefab(Units unit)
    {
        // Convert to int, use as an index.  If it's out of bounds default to the first item in the whole list.
        // NOTE: For this purpose at some point put a null, fail-safe unit in the first slot.
        int index = (int)unit;

        if (UnitCatalog.instance.unitCatalog.Count < index)
        {
            if (UnitCatalog.instance.unitCatalog.Count == 0) { return null; } // This is gonna be a bad day, library wasn't properlly setup for some reaason

            return UnitCatalog.instance.unitCatalog[0];
        }
        else
        {
            return UnitCatalog.instance.unitCatalog[index];
        }
    }

    public Entity lastSpawnedEntity; // Always link the last spawned entity incase a calling script needs it
    public GameObject SpawnUnit(Units unit, Vector3 position, Quaternion rotation, Entity entityFrom)
    {
        // Instantinate it through photon, pull the prefab name from our library
        UnitCatalogItem unitConfiguration = GetUnitPrefab(unit);

        if (Stats.Purchase(unitConfiguration.Cost))
        {
            // Everything is now prepared to be spawned
            GameObject g = (GameObject)PhotonNetwork.Instantiate(unitConfiguration.Prefab, position, rotation, 0);

            Entity e = g.GetComponent<Entity>();
            lastSpawnedEntity = e;

            if (e)
            {
                // Add this check so user's can specify a Null parent implying it doesn't have a rally point.
                if (entityFrom)
                {
                    // May need to re-adjust, not sure yet.
                    e.MoveTo(entityFrom.settings.RallyPoint);
                }
            }

            // We're going to definetly return our game object as well
            return g;
        }
        else
        {
            return null;
        }
       
    }
}

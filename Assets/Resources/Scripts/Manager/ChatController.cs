﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// This is a Photon based mono behaviour object that we use to sync with the chat in the current room we're connected to
public class ChatController : Photon.MonoBehaviour
{
    public bool Live = false;

    public List<string> chatEntryBuffer = new List<string>();

    public string ChatCache = ""; // Contains a "Cached" version of the text to actually display for chat

    // Make this class a singleton
    private static ChatController _instance;
    public static ChatController instance { get { if (_instance == null) { _instance = GameObject.FindObjectOfType<ChatController>(); } return _instance; } }

	// Use this for initialization
	void Start () {
	    // Check for connection
        Live = PhotonNetwork.connected;
	}
	
	// Update is called once per frame
    int reconnectionTimer = 0;
	void Update () {
	    if (Live == false)
        {
            // Delay a check for connection again, if we spam it it'll bog down resources
            reconnectionTimer++;

            if (reconnectionTimer >= 260)
            {
                Live = PhotonNetwork.connected;
            }
        }
        else
        {
            // Update our chat!

            // At the end of the update check if we need to rebuild the cache
            rebuildCache();
        }
	}

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        // Not used, but keeps photon happy.
    }

    int cacheLastSize = 0;
    void rebuildCache()
    {
        // If the buffer has changed size rebuild our cache
        if (chatEntryBuffer.Count != cacheLastSize)
        {
            cacheLastSize = chatEntryBuffer.Count;

            string buffer = "";

            for (int i = 0; i < chatEntryBuffer.Count; i++)
            {
                buffer = buffer + chatEntryBuffer[i] + "\n";
            }

            ChatCache = buffer;
        }
    }

    // The bread and butter (Other then the cache I suppose) of this nifty little chat manager.  Simple In and out, a nice send message function and a string that you can safely pull as often as you'd like!
    public void SendChat(string content)
    {
        // Make an RPC call to all connected players
        for (int i= 0; i < PhotonNetwork.playerList.Length; i++)
        {
            photonView.RPC("PhotonRecieveMessage", PhotonNetwork.playerList[i], content);
        }
    }

    [RPC]
    void PhotonRecieveMessage(string text, PhotonMessageInfo info)
    {
        chatEntryBuffer.Add("[37e843]" + info.sender.name + "[-]: " + text);

        // Check for enabling/disabling of cheats and settings
        if (text == "allowCheats".ToLower())
        {
            GlobalConfiguration.AllowCheats = true;
        }

        if (text == "ShowMe".ToLower())
        {
            GlobalConfiguration.ShowEnemyOnMinimap = true;
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class ParticleManagerSync : Photon.MonoBehaviour {

    public void SendParticle(int id, Vector3 location)
    {
        string buffer = id.ToString() + "|" + NetworkTools.Vector3ToString(location);

        // Send it to all other clients
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            if (!PhotonNetwork.playerList[i].isLocal)
            {
                photonView.RPC("RecieveParticle", PhotonNetwork.playerList[i], buffer);
            }
        }
    }

    [RPC]
    public void RecieveParticle(string data, PhotonMessageInfo info)
    {
        string[] bits = data.Split('|');

        // Pull the ID and location then spawn this locally.
        try
        {
            int id = int.Parse(bits[0]);
            Vector3 loc = NetworkTools.Vector3FromString(bits[1]);

            ParticleManager.instance.PlayParticleLocal(id, loc);
        }
        catch (System.Exception z) { }
    }
}

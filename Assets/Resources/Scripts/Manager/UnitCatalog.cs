﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitCatalog : MonoBehaviour {

    private static UnitCatalog _instance;
    // This puppy makes sure that when we're statically called from other scripts that we make sure this isn't a null version of the General Controller.
    public static UnitCatalog instance { get { if (_instance == null) { _instance = GameObject.FindObjectOfType<UnitCatalog>(); } return _instance; } }

    public enum Units
    {
        CUBE_Command,
        CUBE_Factory,
        CUBE_Flackjack,
        CUBE_Hammerhead,
        CUBE_Infantry,
        CUBE_Razorback,
        CUBE_Worker,
        CUBE_Rifleman,
        CUBE_Gernader,
		TRI_Command,
		TRI_Barracks,
		TRI_Hail,
		TRI_Infantry,
		TRI_Mortar,
		TRI_Trebuche,
		TRI_Glaive,
		TRI_Devourer,
		TRI_Turret,
		CUBE_Turret,
		TRI_Worker,
		CIR_Command,
		CIR_Barracks,
		CIR_Ama,
		CIR_Healer,
		CIR_Infantry,
		CIR_Reaper,
		CIR_Saucer,
		CIR_Worker,
		CIR_Turret,
		TRI_Dragonfly,
		CIR_Titan,
		CIR_Dragoon,
		CIR_Rotatron,
		CUBE_RavenLight
    }

    public GameObject[] UnitPrefabs;

    [HideInInspector]
    public List<UnitCatalogItem> unitCatalog = new List<UnitCatalogItem>();

    void Awake()
    {

    }

    public void PrepareList()
    {
        // Compile our catalog from the given prefabs

        for (int i = 0; i < UnitPrefabs.Length; i++)
        {
            UnitCatalogItem u = new UnitCatalogItem();
            u.Prefab = UnitPrefabs[i].name;

            // Find and get the cost
            EntitySettings e = UnitPrefabs[i].GetComponent<EntitySettings>();
            u.Cost = e.Cost;

            unitCatalog.Add(u);
        }
    }
    
}


public class UnitCatalogItem
{
    public string Prefab = "";
    public int Cost = 0;
}
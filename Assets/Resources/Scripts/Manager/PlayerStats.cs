﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerStats : MonoBehaviour {

    private PlayerStatsPhoton playerStatsPhoton; // Our photon/network interface counter part
    public bool hasPhotonInterface { get { return (playerStatsPhoton); } } // Gives us a check for if we've setup or currently have a PhotonObject connected
    public List<PSS> pssCache = new List<PSS>();

    public bool DisplayDebugText = false;

    public PSS _player { get { return pssCache[0]; } set { pssCache[0] = value; } } // Linking object for our player entry in the cache
    private GUIText debugGUIText;
    private string lastStatEncap = "";

    private float previousTotalIncome = 0;
    private System.DateTime previousTotalTime = System.DateTime.Now;

    #region Singleton Stuff
    /////////////////////////////////////////////////
    ////////////// Start of our Singleton solution
    /////////////////////////////////////////////////

    private static PlayerStats _instance;
    public static PlayerStats instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PlayerStats>();

                //Tell unity not to destroy this object when loading a new scene!
                if (_instance)
                {
                    DontDestroyOnLoad(_instance.gameObject);
                }
            }

            return _instance;
        }
    }

    public static PSS Player
    {
        get
        {
            return instance._player;
        }
    } // Shorter link from global static down to internal player link.

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);

            // Past this will actually server as our initalization of the object.

            // Clean pretty much covers initalization
            Clean();

            debugGUIText = GetComponent<GUIText>();

            // When this object is setup we'll want to actually spawn in our own network interface for this, this will also allow us to communicate out with other players to sync stats.
            PrepareInterface();
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    /////////////////////////////////////////////////
    ////////////// End of our Singleton solution
    /////////////////////////////////////////////////
    #endregion


    public string GetDebugString()
    {
        // Build a string of info for our player stats.  Mostly for debugging purposes.
        string buffer = "";

        for (int i = 0; i < pssCache.Count; i++)
        {
            string entry = "[" + pssCache[i].Name + "]\n";
            entry += "-- Latency:    " + pssCache[i].Ping.ToString() + "\n";
            entry += "-- FPS:        " + pssCache[i].FPS.ToString() + "\n";  
            entry += "-- Units:      " + pssCache[i].UnitCount.ToString() + "\n";
            entry += "-- Vectorites: " + pssCache[i].Vectorites.ToString() + "\n";
            entry += "-- Yield:      " + pssCache[i].Yield.ToString() + "\n";
            entry += "-- Camera:     " + pssCache[i].CameraLocation.ToString() + "\n";
            entry += "-- Color:      " + pssCache[i].Color.ToString() + "\n";

            entry += "========\n";

            buffer += entry;
        }

        buffer += "\n===========\nPacket Info\n===========\n";
        buffer += "Packet Package Count: " + EntityPacketManager.instance.futureBuffers.Count.ToString();

        return buffer;
    }

    public string GetLatencyString()
    {
        string buffer = "";

        for (int i = 0; i < pssCache.Count; i++)
        {
            buffer += "[50d443][" + pssCache[i].Name + "][-] -- " + pssCache[i].Ping.ToString() + "\n";
        }

        return buffer;
    }

    public void RecieveMessage(string data)
    {
        // Incoming string from other player's PSS.

        // Make it into a PSS
        PSS incomingData = new PSS(data);

        // Find who's data this is and update them, otherwise if they're not found make a new entry.
        bool foundPlayer = false;

        for (int i = 0; i < pssCache.Count; i++)
        {
            if (pssCache[i].Name == incomingData.Name)
            {
                // Update em
                pssCache[i].FromString(data);
                foundPlayer = true;
            }
        }

        if (!foundPlayer)
        {
            // First time we've heard from them, make an entry
            pssCache.Add(incomingData);
        }
    }

    DateTime lastFPSCheck = DateTime.Now; // Used to defer FPS check's

    // These 3 functions are utility's for spawn locations, shortcuts for picking an empty random slot, A general method for setting it, and an
    // integrity check that makes sure 2 people don't have the same spawn locaiton.
    public void CheckSpawnLocationIntegrity()
    {
        bool match = false;

        if (pssCache.Count <= 1)
        {
            // Just you
            return;
        }

        // See if anyone has the same spawn location as us
        for (int i= 1; i < pssCache.Count; i++) // A starting index of 1 skips us
        {
            if (pssCache[i].SpawnSlot == Player.SpawnSlot)
            {
                match = true;
            }
        }

        // If there was a match, then pick a new spawn location
        if (match)
        {
            PickSpawnLocation();
        }
    }

    public void PickSpawnLocation()
    {
        // Find an index
        int findIndex = RandomEmptySpawnLocation();

        // Save it
        Player.SpawnSlot = findIndex;
    }

    public int RandomEmptySpawnLocation()
    {
        bool found = false;
        int index = 0;

        while (found == false)
        {
            bool hasSlot = false;

            for (int i = 0; i < pssCache.Count; i++)
            {
                if (pssCache[i].SpawnSlot == index)
                {
                    hasSlot = true;
                }
            }

            if (hasSlot == false)
            {
                found = true;
                return index; // We found one
            }
            else
            {
                // Otherwise try another random index.
                index = UnityEngine.Random.Range(0, 4);
            }
        }

        return index;
    }

    public void RandomizePlayerColor()
    {
        // Set a new random color
        Player.Color = new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value, 1.0f);
    }
	
	void Update () {

        CheckDebugToggle();

        // Check spawn location integrity
        CheckSpawnLocationIntegrity();

        // Debug data stuff ->
        if (DisplayDebugText)
        {
            debugGUIText.enabled = true;

            // Update it's text
            string buffer = GetDebugString();
            buffer += "\nYour camera speed - " + GlobalConfiguration.CameraMoveSpeed.ToString();

            debugGUIText.text = buffer;
        }
        else
        {
            debugGUIText.enabled = false;
        }

        // Keep checking to make sure our Photon interface hasn't been scrapped by a scene change
        PrepareInterface();

        // Update our info
        Player.Name = PhotonNetwork.playerName;
        Player.Ping = PhotonNetwork.GetPing();

        // Every second recalculate our Yield
        if ((System.DateTime.Now - previousTotalTime).Seconds >= 1)
        {
            previousTotalTime = System.DateTime.Now;

            // Calculate the total vectorite change in that second, and record it
            float newYield = Player.TotalIncome - previousTotalIncome;

            yieldStoreYield(newYield);

            previousTotalIncome = Player.TotalIncome;

            // Apply our current yield
            Player.Yield = getCurrentYield();
        }

        // Calculate our yield
        
        // Only update our FPS every like 5 seconds
        if ((DateTime.Now - lastFPSCheck).Seconds > GlobalConfiguration.NetworkFPSCheckTime)
        {
            lastFPSCheck = DateTime.Now;
            Player.FPS = 1.0f / Time.smoothDeltaTime;
        }

        // The following information is set/managed by other scripts so we'll transparently send out updates as it changes
        // Vectorites/Active Units.

        // Now we should do an encapsulation check, while it may seem like a waste of resources to build our string every frame it's still
        // worth the possible save in network resources to only send updates on value changes
        string currentEncap = Player.ToString();

        if (currentEncap != lastStatEncap)
        {
            // Change in data!

            // Call to update
            if (playerStatsPhoton)
            {
                playerStatsPhoton.UpdatePSSData();
            }

            lastStatEncap = currentEncap;
        }
	}

    // Utility functions
    public PSS GetPSS(string name)
    {
        // Find a PSS from our cache with the give name
        for (int i = 0; i < pssCache.Count; i++)
        {
            if (pssCache[i].Name == name)
            {
                return pssCache[i];
            }
        }

        return new PSS(); // Just shoot back an empty one, idk.
    }

    public void CheckDebugToggle()
    {
        // If shift and tab were pressed then toggle the debug display
        if (Input.GetKeyDown(KeyCode.Tab) && Input.GetKey(KeyCode.LeftShift))
        {
            DisplayDebugText = !DisplayDebugText;
        }
    }

    public void Clean()
    {
        // It'll be a good idea to clean this up from time to time
        pssCache.Clear();

        SetupLocal();

        // Now attach up to the Stats interface and prepare it
        Stats.playerStats = this;
        Stats.Prepare();
        
    }

    public void SetupLocal()
    {
        // Only call after cleaning up or on initialization.  Makes our first PSS entry and records the index of the local player
        PSS localPlayer = new PSS();
        localPlayer.Name = PhotonNetwork.playerName;

        localPlayer.Color = new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value, 1.0f);

        // Clear our list
        pssCache.Clear();

        // Make it the root object in our cache always, can access with the Player link
        pssCache.Add(localPlayer);
    }

    public void PrepareInterface()
    {
        if (!hasPhotonInterface)
        {
            // We need a new interface

            // Call on Photon to spawn one network side
            GameObject g = PhotonNetwork.Instantiate("PlayerStatistics", Vector3.zero, Quaternion.identity, 0);

            // Link up the component from it
            playerStatsPhoton = g.GetComponent<PlayerStatsPhoton>();
        }
    }

    /////////////////////////////////////
    // Yield related utilities/stuff
    List<float> yieldHistory = new List<float>();
    public void yieldStoreYield(float value)
    {
        yieldHistory.Add(value);

        if (yieldHistory.Count > 30) // The larger, the long the average spans
        {
            yieldHistory.RemoveAt(0);
        }
    }

    public float getCurrentYield()
    {
        if (yieldHistory.Count == 0) { return 0; }

        float t = 0;

        foreach (float v in yieldHistory) { t += v; }

        float result = t / yieldHistory.Count;

        return (int)result;
    }
}

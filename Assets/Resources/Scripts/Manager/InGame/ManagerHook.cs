﻿using UnityEngine;
using System.Collections;

public class ManagerHook : MonoBehaviour {

	// This is just a unity object for giving our global static scripts an interface and place to actually inherit the update loop from unity.
	
	void Start () {
		EntityManager.Start ();
	}

	void Update () {

		// Entity manager
		Entity[] es = FindObjectsOfType(typeof(Entity)) as Entity[];
		EntityManager.Update (es);
	}
}

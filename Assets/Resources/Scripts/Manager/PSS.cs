﻿using UnityEngine;
using System.Collections;

public class PSS  {

    public enum PlayerSide { Cube, Sphere, Triangle }

    // PSS stands for PlayerStatsShell, it's a data type for use with the PlayerStats management to standardize and simplify the collection of information for each player

    // Public Setter/Getter variables
    public string Name { get { return _name; } set { _name = value; } }
    public float FPS { get { return _fps; } set { _fps = value; } }
    public PlayerSide Side { get { return _side; } set { _side = value; } }
    public Color Color { get { return _color; } set { _color = value; } }
    public float Ping { get { return _latency; } set { _latency = value; } }
    public int Vectorites { get { return _vectorites; } set { _vectorites = value; } }
    public int TotalIncome { get { return _totalIncome; } set { _totalIncome = value; } }
    public int UnitCount { get { return _UnitCount; } set { _UnitCount = value; } }
    public Vector3 CameraLocation { get { return _cameraLocation; } set { _cameraLocation = value; } }
    public int SpawnSlot { get { return _spawnSlot; } set { _spawnSlot = value; } }
    public float Yield { get { return _yield; } set { _yield = value; } }

    public bool Alive = true;
    public int Faction = 0; // 0 = Cubiote, 1 - Spherog, 2 - Trigun

    // Privatelly held info
    private string _name = "default";
    private float _fps = 0;
    private PlayerSide _side = PlayerSide.Cube;
    private Color _color = Color.white;
    private float _latency = 0; // Should be in MS, or whatever Photon defaults to for now.
    private int _vectorites = 0;
    private int _UnitCount = 0;
    private Vector3 _cameraLocation = Vector3.zero;
    private int _spawnSlot = 0;
    private float _yield = 0;
    private int _totalIncome = 0;

    // Constructors
    public PSS()
    { }

    public PSS(string Name, PlayerSide Side, Color Color)
    {
        _name = Name;
        _side = Side;
        _color = Color;
    }

    public PSS(string Name, PlayerSide Side)
    {
        _name = Name;
        _side = Side;
    }

    // Constructor for previously encapsilated data
    public PSS(string importData)
    {
        FromString(importData);
    }


    // Utility functions for encapsiating and decapsilitating data
    public string ToString()
    {
        string buffer = "";

        // Combine up our data
        buffer += Name + "|";
        buffer += Vectorites.ToString() + "|";
        buffer += UnitCount.ToString() + "|";
        buffer += FPS.ToString() + "|";
        buffer += _latency.ToString() + "|";
        buffer += ((int)_side).ToString() + "|";
        buffer += NetworkTools.Vector3ToString(_cameraLocation) + "|";
        buffer += NetworkTools.ColorToString(_color) + "|";
        buffer += SpawnSlot.ToString() + "|";
        buffer += Yield.ToString() + "|";
        buffer += Alive.ToString() + "|";
        buffer += Faction.ToString();

        return buffer;
    }

    public void FromString(string data)
    {
        string[] bits = data.Split('|');

        try
        {
            // Apply the incoming data
            _name = bits[0];
            _vectorites = int.Parse(bits[1]);
            _UnitCount = int.Parse(bits[2]);
            _fps = float.Parse(bits[3]);
            _latency = float.Parse(bits[4]);
            _side = (PlayerSide)int.Parse(bits[5]); // The enum should be transfered as it's integer value.
            _cameraLocation = NetworkTools.Vector3FromString(bits[6]);
            _color = NetworkTools.ColorFromString(bits[7]);
            _spawnSlot = int.Parse(bits[8]);
            _yield = float.Parse(bits[9]);
            Alive = bool.Parse(bits[10]);
            Faction = int.Parse(bits[11]);
        }
        catch(System.Exception z)
        {
            // Don't have a hissy on corrupt incoming data, just ignore it.
        }
    }
}

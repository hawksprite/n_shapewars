﻿using UnityEngine;
using System.Collections;

public class PlayerStatsPhoton : Photon.MonoBehaviour {

    // This is the Photon based counter part to the Player Stats object.  Just a seperate location to stick all of our Network SHIM's to keep the main logic clean.
    private PlayerStats playerStats
    {
        get
        {
            return PlayerStats.instance;
        }
    }

	void Start () {
        // If locally owned this is our Network interface!
        if (this.networkView.isMine)
        {
            // We don't need to link back because the parent makes the link on spawning of it's own Photon object.
        }
	}
	
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        // We're not serializing these objects, but it needs to be here to keep photon happy.
    }

    [RPC]
    void PSSDataUpdate(string data, PhotonMessageInfo info)
    {
        // On recieving this update alert the new data to this scripts local playerStats;
        playerStats.RecieveMessage(data);
    }

    public void UpdatePSSData()
    {
        // Make an update of our PSS data.

        string buffer = PlayerStats.Player.ToString(); // Encapsulate our players data.

        // Make an RPC call to everyone else
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            if (!PhotonNetwork.playerList[i].isLocal)
            {
                // Send it out to all non locals.
                photonView.RPC("PSSDataUpdate", PhotonNetwork.playerList[i], buffer);
            }
        }
    }


}

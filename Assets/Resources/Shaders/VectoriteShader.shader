﻿Shader "Custom/VectoriteShader" {
	Properties {
		_MainColor("Base Color", Color) = (1,1,1,1)
		_RimColor("Rim Color", Color) = (1, 1, 1, 1)
		_RimPower("Rim Power", Range(1.0, 6.0)) = 3.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
		};

		float4 _RimColor;
		float _RimPower;
		float4 _MainColor;

		void surf (Input IN, inout SurfaceOutput o) {
			//IN.color = _ColorTint;

			o.Albedo = _MainColor;
			
			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			o.Emission = _RimColor.rgb * pow(rim, _RimPower);
		}
		ENDCG
	} 
	FallBack "Diffuse"
}

// Shader created with Shader Forge Beta 0.28 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.28;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:True,rmgx:True,hqsc:True,hqlp:False,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|diff-42-OUT,spec-13-OUT,normal-6-RGB;n:type:ShaderForge.SFN_Tex2d,id:4,x:33222,y:32579,ptlb:Diffuse_G,ptin:_Diffuse_G,tex:5a378c3a883fca84581a7efcbc2ef487,ntxv:0,isnm:False|UVIN-44-OUT;n:type:ShaderForge.SFN_Tex2d,id:5,x:33222,y:32932,ptlb:Specular_G,ptin:_Specular_G,tex:800d2294fc5ea224d94785014f4ec6ff,ntxv:0,isnm:False|UVIN-44-OUT;n:type:ShaderForge.SFN_Tex2d,id:6,x:33222,y:33183,ptlb:Normal_G,ptin:_Normal_G,tex:618d46313209ffa43bea1cd04b165af6,ntxv:3,isnm:True|UVIN-44-OUT;n:type:ShaderForge.SFN_ValueProperty,id:12,x:33222,y:32820,ptlb:SpecularAmmount,ptin:_SpecularAmmount,glob:False,v1:0.9;n:type:ShaderForge.SFN_Multiply,id:13,x:33059,y:32838|A-12-OUT,B-5-RGB;n:type:ShaderForge.SFN_TexCoord,id:24,x:33925,y:32745,uv:0;n:type:ShaderForge.SFN_Color,id:41,x:33222,y:32390,ptlb:Color,ptin:_Color,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:42,x:32975,y:32477|A-41-RGB,B-4-RGB;n:type:ShaderForge.SFN_ValueProperty,id:43,x:33859,y:33012,ptlb:UV_Multiply,ptin:_UV_Multiply,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:44,x:33680,y:32858|A-24-UVOUT,B-43-OUT;proporder:4-5-6-12-41-43;pass:END;sub:END;*/

Shader "Shader Forge/DemoFloorShader" {
    Properties {
        _Diffuse_G ("Diffuse_G", 2D) = "white" {}
        _Specular_G ("Specular_G", 2D) = "white" {}
        _Normal_G ("Normal_G", 2D) = "bump" {}
        _SpecularAmmount ("SpecularAmmount", Float ) = 0.9
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _UV_Multiply ("UV_Multiply", Float ) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Diffuse_G; uniform float4 _Diffuse_G_ST;
            uniform sampler2D _Specular_G; uniform float4 _Specular_G_ST;
            uniform sampler2D _Normal_G; uniform float4 _Normal_G_ST;
            uniform float _SpecularAmmount;
            uniform float4 _Color;
            uniform float _UV_Multiply;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_44 = (i.uv0.rg*_UV_Multiply);
                float3 normalLocal = UnpackNormal(tex2D(_Normal_G,TRANSFORM_TEX(node_44, _Normal_G))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i)*2;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor + UNITY_LIGHTMODEL_AMBIENT.xyz*2;
///////// Gloss:
                float gloss = exp2(0.5*10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = (_SpecularAmmount*tex2D(_Specular_G,TRANSFORM_TEX(node_44, _Specular_G)).rgb);
                float3 specular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),gloss) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * (_Color.rgb*tex2D(_Diffuse_G,TRANSFORM_TEX(node_44, _Diffuse_G)).rgb);
                finalColor += specular;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Diffuse_G; uniform float4 _Diffuse_G_ST;
            uniform sampler2D _Specular_G; uniform float4 _Specular_G_ST;
            uniform sampler2D _Normal_G; uniform float4 _Normal_G_ST;
            uniform float _SpecularAmmount;
            uniform float4 _Color;
            uniform float _UV_Multiply;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_44 = (i.uv0.rg*_UV_Multiply);
                float3 normalLocal = UnpackNormal(tex2D(_Normal_G,TRANSFORM_TEX(node_44, _Normal_G))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i)*2;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor;
///////// Gloss:
                float gloss = exp2(0.5*10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = (_SpecularAmmount*tex2D(_Specular_G,TRANSFORM_TEX(node_44, _Specular_G)).rgb);
                float3 specular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),gloss) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * (_Color.rgb*tex2D(_Diffuse_G,TRANSFORM_TEX(node_44, _Diffuse_G)).rgb);
                finalColor += specular;
/// Final Color:
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System;

[ExecuteInEditMode]
public class SavePreview : MonoBehaviour {

    [ContextMenu("Save Image")]
    void saveImage()
    {
        UnityEngine.Object target = Selection.activeObject;

        // Get the image
        Texture2D previewImage = AssetPreview.GetAssetPreview(target);

        string saveLocation = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/" + target + ".png";

        Debug.Log(saveLocation);

        byte[] byteBuffer = previewImage.EncodeToPNG();
        FileStream file = File.Open(saveLocation, FileMode.Create);
        BinaryWriter binary = new BinaryWriter(file);

        // Write it in
        binary.Write(byteBuffer);

        file.Close(); // Clean up
        
    }
}

NEON_Shapewars
==============

Instructions:
==============
Introduction: Hello! Welcome to Shape Wars! Shape Wars is an RTS (real-time strategy) game in which you select a faction of shapes and wage polygonal war on each other to find out who is the best shape of them all! The three available factions are the Cubite, Trigon, and Spherog and each comes with their own unique characteristics. There are many strategies with each faction so experiment with them and have fun playing!

How to Play: Since Shape Wars is a real-time strategy game, player actions happen in real-time. You as the player will need to react to your opponents decisions on the battlefield and plan your battle strategies around their actions. The main goal of the game is to take out your opponent’s base of operations and units before they can take yours out. 
Movement and selection: In order to move the main combat forces of the shape factions, you need to left-click on a unit to select it. A blue transparent circle will pop up and show you where the range of attack for the unit is located. Movement works by right-clicking on the map with a unit selected. It is possible to select more than one unit at a time and give directional orders to them in tandem. Navigation can also be performed by right-clicking on the mini-map in the bottom left hand corner of the screen. 

Command List: Each unit has a small list of actions that can be performed by clicking on them. These actions are in the bottom-right hand corner of the screen. Each unit has specific actions that pertain to them. Most actions can be summed up in combat, building, and support functions. The names and unit cost of the units will be displayed in the workers command list when you hover your mouse over them. 

Combat/Game Setup: The main combat of the game is carried out through units. Units are purchased using the in-game currency called vectorite. Vectorite appears as blue spikes coming up out of the map, it is mined by assigning worker units to collect resources and bring the vectorite back to your base. The player will usually start the match with 500 vectorite. This is enough to buy 5 worker units from your base of operations and set them to collect resources. To build a worker, simply left-click on the base of operations, then left-click on the first icon in the bottom right-hand corner of the screen. Once the worker units begin collecting enough resources, you can then select one or multiple workers and right click to drag them away from resource collecting. This will allow you to build factories, which grant the player the ability to make combat units. The factory button is the first unit that is selectable in the command list for the worker unit. To set the worker back to collecting vectorite, simply left-click on the gold ore icon in the command list for the worker. Once a factory has been built, it can be selected and you can build a variety of units based on your faction, as long as you have the required vectorite. Factories are not indestructible, meaning that your opponents can take them out and leave you without a way to produce units. Once some new units have been built, you can mobilize your units at one of your opponents and wage shape war!

Win Condition: The game is won when all other players other than yourself have lost all their units and bases. 

Lose condition: The game is lost when you lose all of your units and bases.       



Story: 
==============

  The planet Polygonus, home to organic living shapes that have evolved over the millennia to survive on their home world. The life force and most abundant resource on the planet is vectorite. For a long time this resource was quite abundant and could be found just about anywhere on the planet. Through the centuries the shapes on the planet’s surface changed from basic geometric proportions to fully three-dimensional beings. This transformation was helped greatly by the natural resource of Polygonus. The vectorite had an effect on the shapes that caused them to grow in conjunction with their planet. 
  
  The beginning of the planet’s life forms resembled simple two-dimensional lines and dots. As time went on these lines and dots began to form simple two-dimensional shapes such as triangles, squares, rhombi, parallelograms, trapezoids, etc. The most common of the shapes that were created from this gradual change were the triangles and the squares. These two shapes came to be the two largest and most dominant species on the planet. As their numbers grew so did their definition in shape. The flat two-dimensional bodies of the past began to gain three-dimensional definition and take a more three-dimensional form. This change also produced more knowledge for the shapes as the simple brain patterns of their two-dimensional forms gave way to more complex thought and social reasoning. 
  
  Over the centuries as the shapes became smarter and more three-dimensional, they began to form tribes. The other shapes that did not adapt and grow more intelligent either stayed two-dimensional or died off and became part of the Great Vector Cloud in the core of the planet.  The early tribes of the triangles and cubes were peaceful towards one another. They built huts for one another, helped rear each other’s children, and looked after each other’s elderly before they passed on. The peace lasted for many hundreds of years. That is, until the squares began to experiment more heavily with vectorite in its crystal form that they had been finding from their excavations for food and other resources. They had found that infusing their life essence with these crystals gave them bursts of frenetic brainpower, which they used to build better machines and defenses from wild shapes for the triangle and square society. Some improvements that the squares brought were higher and more fortified walls and primitive tanks that could protect squares and triangles while helping on their hunting expeditions. This brought a temporary boost to the lifestyles of both the shapes and both of their lives improved immensely. The tribes disappeared and cities and townships began to be built and the safety of the community increased triple-fold. 

  However, because the squares were the ones with the increased cranial capacity now, the triangles began to be treated like second class citizens. The squares enjoyed the superiority that they had over their triangle brethren, especially since over a few decades it had almost changed them into full cubes. The Cubites as they now referred to themselves wanted to make sure that their triangle citizens would never be able to gain access to the increased knowledge that they had gained from the vectorite crystals. They set up laws to keep the triangles ignorant and submissive. This did not sit well with the triangles. A rebel group known as the Triple Pointed Sword was formed in resistance to the Cubites. 
The Triple Pointed Sword became more widespread and their infectious message of freedom and knowledge spread to other neighboring triangle communities. The Triple Pointed Sword started to attack the Cubite strongholds in secret. The attacks were little at first, stealing confidential plans, taking small amounts of food and water, and performing small amounts of vandalism to the Cubite fortresses. Soon enough though, these small attacks were not enough. A large full scale assault was planned and the goal of the attack was to gain some of the vectorite crystals that the Cubites were hoarding for themselves. 

  What came to be known to the triangles as The Glorious Night of Freedom was known to the Cubites as The Night of Ultimate Betrayal. The Triple Pointed Sword and many enthusiastic and pointy volunteers stormed the castle and stole a vast amount of the vectorite crystals. This was not without consequence, however, as there were massive triangle casualties in the raid due to the Cubites superior weapon technology. The Cubites, fearing an uprising from the triangles built up a massive arsenal of tanks and other land based units to fight the triangle with. The sheer number of triangles allowed them to make off with the vectorites they so desperately wanted. 
  
  The Cubites counted their losses after the battle and resolved to hunt down the triangles responsible for taking their precious vectorite. They decided to give the triangles an ultimatum, give the Cubites back their vectorite and leave their land or be destroyed. However, the triangles had anticipated this course of action as they had already begun to pack up their things and leave for a new land that very night. 
  
  The Cubites arrived at the triangle settlements only to find that they had already left without a trace. Over the next few years the triangles began to experiment with the vectorite crytals in much the same way as the Cubites had. They had discovered a more efficient way to mine the vectorite out of the planet’s surface by using convoy vehicles. The new mining techniques allowed the triangles to advance even more rapidly than the Cubites had. They became more pyramidal and started to associate themselves by the name of Trigons. The Trigons did not rest on their laurels after their great theft. They began to create weapon technology of their own to combat the possibility of Cubite vengeance. Since the Trigons knew that the Cubites would need to travel a great distance to reach them, the Trigons developed long range and turret based weaponry to ensure the advantage of attack.
  
 	The next 3 decades were a time of relative peace between the two species. The Cubites and Trigons worked in their separate communities to build bigger and better weapons and build larger and more structured cities. The peace was only ended when the vectorite began to be in short supply. The Cubites and Trigons needed to expand their influence to new portions of the world in order to continue mining and collecting resources. 
 	
  These expansions led the two species back to each other and their rivalry was started again. Small militia fights for land rights broke out frequently and by the end of about two months no one knew who had a legitimate right to what land anymore. This confusion led the councils of both the Cubites and the Trigons to call a formal meeting of the species to discuss land rights and reparations for the past injustices that they had done to one another. The meeting was held on a sunny day on Polygonus. 

  The meeting was calm and everything seemed to be working out between the two warring species. That is, until a large spherical object descended from the sky. It was massive and it was a shape that the Cubites and Trigons had only managed to make mock-ups of in their buildings and constructing their weapons of war. The sphere descended closer and closer to the council until it stopped dead center among the Cubite and Trigon tables. As it hovered in the air the Cubites and Trigons talked of their strange new visitors. Obviously they were from another planet, but their intentions were not clear from their behavior. They had an unsettling almost mystical air to them even though they looked very technologically advanced. 
  
  Finally the silence was broken as a voice rang out from the sphere. It was an emissary of the species now known to the Cubites and Trigons as the Spherogs. They preached a message of peace and goodwill. Regaling the assembly of shapes with tales of their past exploits and charitable works across the galaxy. Amidst this, one of the Trigon leaders asked the Spherog leader what reason they had for visiting a remote planet such as Polygonus.  
  
  The Spherog then came clean that in order to get back to their home planet, they would need to extract quite a bit of the raw materials of the planet in order to have enough fuel to get home. This news shocked the assembly, as they did not expect their new visitors to demand so much of them. The natural resource of their planet was the lifeblood of their existence. The vectorite had literally made them into the shapes they were today. 
  
  The Cubites and Trigons refused the Spherogs plea for help. However, before the shapes could think, the Spherogs retaliated in anger and frustration by blowing up the nearest Cubite building. The explosion revealed that the Cubites had tanks and ships ready to wipe out the Trigons once the negotiations were over. With this realization, the Trigons fled back to their city to prepare for war. The spherogs flew off to another part of the planet to begin mining procedures anyway. They would get the resources they needed even if they had to fight for them. The Cubites were stunned to find out their plan had gone awry. But they knew that there was no time to rest. They too began to build and stockpile weapons of war to give themselves a fighting chance.      


Credits: 
==============

Project Managers:
Michael Miele
Justin Barnyak

Programming: 
Justin Barnyak
Level Design: 
Michael Miele
Justin Barnyak

Model Development:
Josh Walk
Michael Miele
Ty Brantner

Unit Creation: 
Michael Miele
Justin Barnyak
Josh Walk

Music: 
Ty Branter
Daniel Dean

Sound Effects (Courtesy of Freesound.org): 
Michael Miele


Quality Assurance/Playtesting: 
Michael Miele
Justin Barnyak
Josh Walk
Ty Brantner
Krystalis Kerr
Chris Yaeger
Cody Emery
All of the People in our Gaming and Simulation II class!
All of the People who tested the first build out at the BYOC!

